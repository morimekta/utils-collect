Utilities for Collections
=========================

[![Morimekta](https://img.shields.io/static/v1?label=morimekta.net&message=utils-collect&color=informational)](https://morimekta.net/utils-collect/)
[![Docs](https://www.javadoc.io/badge/net.morimekta.utils/collect.svg)](https://www.javadoc.io/doc/net.morimekta.utils/collect)
[![Pipeline](https://gitlab.com/morimekta/utils-collect/badges/master/pipeline.svg)](https://gitlab.com/morimekta/utils-collect/pipelines)
[![Coverage](https://gitlab.com/morimekta/utils-collect/badges/master/coverage.svg)](https://morimekta.net/utils-collect/jacoco-ut/)
[![License](https://img.shields.io/static/v1?label=license&message=apache%202.0&color=informational)](https://apache.org/licenses/LICENSE-2.0)  
Java module with immutable containers with some specific consistency behaviour requirements, and handy builders.
See [morimekta.net/utils](https://morimekta.net/utils/) for procedures on releases.

## Getting Started

To add to `maven`: Add this line to `pom.xml`under dependencies:

```xml
<dependency>
    <groupId>net.morimekta.utils</groupId>
    <artifactId>collect</artifactId>
    <version>${version}</version>
</dependency>
```

To add to `gradle`: Add this line to the `dependencies` group in `build.gradle`:

```
implementation 'net.morimekta.utils:collect:${version}'
```

## Unmodifiable Containers

This module contains a number of unmodifiable containers with some specific consistency behaviour requirements, and
handy builders. Each of these has handy constructor collectors, and require that all values (and keys) are non-null.
The non-sorted map and set both guarantee that iteration order is kept (as with `LinkedHashSet` and `LinkedHashMap`),
but will also be unmodifiable. The sorted variants have handy no-copy slicing for range subSet and subMap handling.

- `UnmodifiableList`: A list that inherently cannot be modified. It has handy no-copy on slice behavior.
- `UnmodifiableMap`: A hash-map that inherently cannot be modified, and keeps iteration order as it was built.
- `UnmodofiableSet`: A hash-set that inherently cannot be modified, and keeps iteration order as it was built.
- `UnmodifiableSortedMap`: An array based sorted map, that inherently cannot be modified.
- `UnmodifiableSortedSet`: An array based sorted set, that inherently cannot be modified.
- `ListBuilder`, `SetBuilder` `MapBuilder`: Base interfaces for simple builders of said base types.

```java
import java.util.List;
import java.util.Set;

import static net.morimekta.collect.UnmodifiableSortedSet.toSortedSet;

interface Example {
    // Get the ID of the example.
    String getName();

    // Get a sorted set of the names of the examples.
    static Set<String> getNames(List<Example> examples) {
        return list.stream()
                   .map(Example::getName)
                   .collect(toSortedSet());
    }
}
```

## Extra Collectors

See the `ExtraCollectors` class, with a set of handy collectors:

- `inBatchesOf`: Split the stream into a list of lists of max size N, each inner list has a maximum result size from the
  batch.
- `inNumBatches`: Split the stream into a list of N lists, each with close to the same size.
- `groupingBy`: Group items in the stream by key, optionally map value.
- `groupingByAll`: Group items in the stream by a set of keys, each item put under *all* keys provided.

```java
import java.util.List;

import static net.morimekta.collect.util.ExtraCollectors.inBatchesOf;

interface Job extends Runnable {
    static void doJobs(List<Job> jobs) {
        jobs.stream()
            .collect(inBatchesOf(10))
            .forEach(batch -> {
                // do something with batch of 1..10 Job items.
            });
    }
}
```

## Utilities

- `Binary`: Wrapping a byte array in an immutable container with handy utilities and no-copy slicing behavior.
- `Pair` & `Tuple`: Simple unmodifiable pair and tuple classes to encompass a pair of values or a short tuple with basic
  helpers and operations.
- `LazyCachedSupplier`: Supplier that will lazily cache from another supplier and keep that value. Works similar to the
  kotlin `lazy` keyword.
- `SetOperations`: Contains a number of standard set operations, all which follow the standard mathematical set
  operations explained e.g.
  in [Encyclopedia Britannica](https://www.britannica.com/science/set-theory/Operations-on-sets)
  or [Probability Course](https://www.probabilitycourse.com/chapter1/1_2_2_set_operations.php).
