/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect;

import java.util.Collection;
import java.util.List;

/**
 * Interface for building lists with a fluent syntax. The builder is purely additive,
 * as if full list operations are needed, a true {@link List} implementation should
 * be used instead.
 *
 * @param <I> The list item type.
 */
public interface ListBuilder<I> {
    /**
     * Add a single item to the end of the list.
     *
     * @param value The item to add.
     * @return The builder.
     */
    ListBuilder<I> add(I value);

    /**
     * Add all items in collection to the end of the list in order.
     *
     * @param items Collection with items to be added.
     * @return The builder.
     */
    ListBuilder<I> addAll(Collection<? extends I> items);

    /**
     * Build the list.
     *
     * @return The built list.
     */
    List<I> build();
}
