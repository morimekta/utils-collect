/*
 * Copyright (c) 2017, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect.util;

import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.collectors.GroupingByAllCollector;
import net.morimekta.collect.collectors.GroupingByCollector;
import net.morimekta.collect.collectors.InBatchesOfCollector;
import net.morimekta.collect.collectors.InNumBatchesCollector;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collector;

import static java.util.function.Function.identity;

/**
 * Extra collector utilities.
 */
public final class ExtraCollectors {
    /**
     * Collect into batches of max N items per batch. Creates a list of lists as response.
     *
     * @param itemsPerBatch Maximum number of items per batch.
     * @param <T> The item type.
     * @return The list of batched entries.
     */
    public static <T> Collector<T, ?, List<List<T>>> inBatchesOf(final int itemsPerBatch) {
        return new InBatchesOfCollector<>(itemsPerBatch);
    }

    /**
     * Collect into N batches of approximate equal size. Creates a stream of lists as response.
     *
     * @param numBatches Number of batch to split between.
     * @param <T> The item type.
     * @return The list of batched entries.
     */
    public static <T> Collector<T, ?, List<List<T>>> inNumBatches(int numBatches) {
        return new InNumBatchesCollector<>(numBatches);
    }

    /**
     * Group items by key. A difference with the native {@link java.util.stream.Collectors#groupingBy(Function)}
     * is that this allows duplicate keys, where the last value mapping to that
     * key to be used.
     *
     * @param toKey Function to get a key for each item.
     * @param <K> The key type.
     * @param <V> The value type.
     * @return The map of key to value.
     */
    public static <K, V> Collector<V, ?, UnmodifiableMap<K, List<V>>>
    groupingBy(Function<V, K> toKey) {
        return groupingBy(toKey, identity());
    }

    /**
     * Group items by key. A difference with the native {@link java.util.stream.Collectors#groupingBy(Function)}
     * is that this allows duplicate keys, where the last value mapping to that
     * key to be used.
     *
     * @param toKey Function to get a key for each item.
     * @param toValue Function to map from input item to value.
     * @param <I> The input item type.
     * @param <K> The key type.
     * @param <V> The value type.
     * @return The map of key to value.
     */
    public static <I, K, V> Collector<I, ?, UnmodifiableMap<K, List<V>>>
    groupingBy(Function<I, K> toKey, Function<I, V> toValue) {
        return new GroupingByCollector<>(toKey, toValue);
    }

    /**
     * Group each item by all values in the grouping by result.
     *
     * @param toKeys Function to get collection of keys to store each value under.
     * @param <K> The key type.
     * @param <V> The value type.
     * @return The map of keys to lists of entries.
     */
    public static <K, V> Collector<V, ?, UnmodifiableMap<K, List<V>>>
    groupingByAll(Function<V, Collection<K>> toKeys) {
        return groupingByAll(toKeys, identity());
    }

    /**
     * Group each item by all values in the grouping by result.
     *
     * @param toKeys Function to get collection of keys to store each value under.
     * @param toValue Function to map from input item to value.
     * @param <I> The input item type.
     * @param <K> The key type.
     * @param <V> The value type.
     * @return The map of keys to lists of entries.
     */
    public static <I, K, V> Collector<I, ?, UnmodifiableMap<K, List<V>>>
    groupingByAll(Function<I, Collection<K>> toKeys, Function<I, V> toValue) {
        return new GroupingByAllCollector<>(toKeys, toValue);
    }

    // PRIVATE constructor to defeat instantiation.
    private ExtraCollectors() {}
}
