/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect;

import java.util.Collection;
import java.util.Set;

/**
 * Interface for building sets with a fluent syntax. The builder is purely additive,
 * as if a full set operations are needed, a true {@link Set} implementation should
 * be used instead.
 *
 * @param <I> The item type.
 */
public interface SetBuilder<I> {
    /**
     * Add a single item to the set. If the item already exist, this does nothing.
     *
     * @param value The item to add.
     * @return The builder.
     */
    SetBuilder<I> add(I value);

    /**
     * Add all items in collection to the set. Ignore any items already in the set.
     *
     * @param items Collection with items to be added.
     * @return The builder.
     */
    SetBuilder<I> addAll(Collection<? extends I> items);

    /**
     * Build the set.
     *
     * @return The built set.
     */
    Set<I> build();
}
