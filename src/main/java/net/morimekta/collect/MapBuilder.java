/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect;

import java.util.Map;

/**
 * Interface for building maps with a fluent syntax. The builder is purely additive,
 * as if a full set of map operations are needed, a true {@link Map} implementation should
 * be used instead.
 *
 * @param <K> The key type.
 * @param <V> The value type.
 */
public interface MapBuilder<K,V> {
    /**
     * Put a single key value pair on to the map. If the key already exists replace
     * the value.
     *
     * @param key The entry key.
     * @param value The entry value.
     * @return The builder.
     */
    MapBuilder<K,V> put(K key, V value);

    /**
     * Put all items in the map to the builder. If any of the keys in the map already
     * exists, then the values for those will be replaced.
     *
     * @param map Map to put items from.
     * @return The builder.
     */
    MapBuilder<K,V> putAll(Map<? extends K, ? extends V> map);

    /**
     * Build the map.
     *
     * @return The built map.
     */
    Map<K,V> build();
}
