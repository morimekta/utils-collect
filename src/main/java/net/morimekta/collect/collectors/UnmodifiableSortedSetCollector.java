/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect.collectors;

import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.UnmodifiableSortedSet;

import java.util.Comparator;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class UnmodifiableSortedSetCollector<E> implements Collector<E, UnmodifiableSortedSet.Builder<E>, UnmodifiableSortedSet<E>> {
    private final Comparator<? super E> comparator;

    public UnmodifiableSortedSetCollector(Comparator<? super E> comparator) {
        this.comparator = comparator;
    }

    @Override
    public Supplier<UnmodifiableSortedSet.Builder<E>> supplier() {
        return () -> UnmodifiableSortedSet.newBuilderOrderedBy(comparator);
    }

    @Override
    public BiConsumer<UnmodifiableSortedSet.Builder<E>, E> accumulator() {
        return UnmodifiableSortedSet.Builder::add;
    }

    @Override
    public BinaryOperator<UnmodifiableSortedSet.Builder<E>> combiner() {
        return (b1, b2) -> b1.addAll(b2.build());
    }

    @Override
    public Function<UnmodifiableSortedSet.Builder<E>, UnmodifiableSortedSet<E>> finisher() {
        return UnmodifiableSortedSet.Builder::build;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return UnmodifiableSet.setOf(Characteristics.UNORDERED);
    }
}