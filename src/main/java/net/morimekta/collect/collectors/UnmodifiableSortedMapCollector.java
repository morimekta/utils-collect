/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect.collectors;

import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.UnmodifiableSortedMap;

import java.util.Comparator;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class UnmodifiableSortedMapCollector<E, K, V> implements Collector<E, UnmodifiableSortedMap.Builder<K, V>, UnmodifiableSortedMap<K, V>> {
    private final Function<E, K> toKey;
    private final Function<E, V> toValue;
    private final Comparator<? super K>  comparator;

    public UnmodifiableSortedMapCollector(Function<E, K> toKey, Function<E, V> toValue, Comparator<? super K> comparator) {
        this.toKey = toKey;
        this.toValue = toValue;
        this.comparator = comparator;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Supplier<UnmodifiableSortedMap.Builder<K, V>> supplier() {
        return () -> (UnmodifiableSortedMap.Builder<K, V>) UnmodifiableSortedMap.newBuilderOrderedBy(comparator);
    }

    @Override
    public BiConsumer<UnmodifiableSortedMap.Builder<K, V>, E> accumulator() {
        return (b, e) -> b.put(toKey.apply(e), toValue.apply(e));
    }

    @Override
    public BinaryOperator<UnmodifiableSortedMap.Builder<K, V>> combiner() {
        return (b1, b2) -> b1.putAll(b2.build());
    }

    @Override
    public Function<UnmodifiableSortedMap.Builder<K, V>, UnmodifiableSortedMap<K, V>> finisher() {
        return UnmodifiableSortedMap.Builder::build;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return UnmodifiableSet.setOf(Characteristics.UNORDERED);
    }
}
