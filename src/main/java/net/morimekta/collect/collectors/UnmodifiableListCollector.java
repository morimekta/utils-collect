/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect.collectors;

import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableSet;

import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class UnmodifiableListCollector<E> implements Collector<E, UnmodifiableList.Builder<E>, UnmodifiableList<E>> {
    @Override
    public Supplier<UnmodifiableList.Builder<E>> supplier() {
        return UnmodifiableList::newBuilder;
    }

    @Override
    public BiConsumer<UnmodifiableList.Builder<E>, E> accumulator() {
        return UnmodifiableList.Builder::add;
    }

    @Override
    public BinaryOperator<UnmodifiableList.Builder<E>> combiner() {
        return (b1, b2) -> b1.addAll(b2.build());
    }

    @Override
    public Function<UnmodifiableList.Builder<E>, UnmodifiableList<E>> finisher() {
        return UnmodifiableList.Builder::build;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return UnmodifiableSet.setOf();
    }
}
