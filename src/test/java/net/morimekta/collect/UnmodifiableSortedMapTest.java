/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect;

import org.junit.jupiter.api.Test;

import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import java.util.UUID;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Comparator.reverseOrder;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSortedMap.newBuilderNaturalOrder;
import static net.morimekta.collect.UnmodifiableSortedMap.newBuilderReverseOrder;
import static net.morimekta.collect.UnmodifiableSortedMap.sortedMapOf;
import static net.morimekta.collect.UnmodifiableSortedMap.toSortedMap;
import static net.morimekta.collect.UnmodifiableSortedSet.sortedSetOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

public class UnmodifiableSortedMapTest {
    @Test
    public void testConvenience() {
        assertThat(Stream.of("a", "b", "c").collect(toSortedMap(s -> s)).keySet(),
                   contains("a", "b", "c"));
        assertThat(Stream.of("a", "b", "c").collect(toSortedMap(s -> s, reverseOrder())).keySet(),
                   contains("c", "b", "a"));
        assertThat(Stream.of("a", "b", "c").collect(toSortedMap(s -> s,
                                                                s -> "" + ((char) (s.charAt(0) + 1)))).values(),
                   contains("b", "c", "d"));
        assertThat(Stream.of("a", "b", "c").collect(toSortedMap(s -> s,
                                                                s -> "" + ((char) (s.charAt(0) + 1)),
                                                                reverseOrder())).values(),
                   contains("d", "c", "b"));

        assertThat(IntStream.range(0, 4096).parallel().boxed().collect(toSortedMap(s -> s)).keySet(),
                   hasSize(4096));
    }

    @Test
    public void testBaseMap() {
        assertThat(sortedMapOf().toString(), is("{}"));
        assertThat(sortedMapOf("bar", "foo").toString(), is("{bar: foo}"));
        assertThat(sortedMapOf("bar", "foo", "foo", "bar").toString(), is("{bar: foo, foo: bar}"));
        assertThat(sortedMapOf("bar", "foo").hashCode(), is(sortedMapOf("bar", "foo").hashCode()));
        assertThat(sortedMapOf("bar", "foo").hashCode(), is(not(sortedMapOf("bar", "bar").hashCode())));

        assertThat(sortedMapOf().equals(sortedMapOf()), is(true));
        assertThat(sortedMapOf("foo", "bar").equals(new HashMap<String, String>() {{
            put("foo", "bar");
        }}), is(true));
        assertThat(sortedMapOf("foo", "bar").equals(new HashMap<String, String>() {{
            put("foo", "foo");
        }}), is(false));
        assertThat(sortedMapOf("foo", "bar").equals(new HashMap<String, String>() {{
            put("foo", null);
        }}), is(false));
        assertThat(sortedMapOf("foo", "bar").equals(new HashMap<String, String>() {{
            put("bar", "bar");
        }}), is(false));

        assertThat(sortedMapOf("foo", "bar").equals(sortedMapOf("foo", "bar")), is(true));
        assertThat(sortedMapOf("foo", "bar").equals(sortedMapOf("foo", "foo")), is(false));
        assertThat(sortedMapOf("foo", "bar").equals(sortedMapOf("bar", "bar")), is(false));
    }

    @Test
    public void testConstructor_Of() {
        assertThat(sortedMapOf(), is(sameInstance(sortedMapOf())));
        assertThat(sortedMapOf().entrySet(), is(sameInstance(sortedSetOf())));
        assertThat(sortedMapOf().values(), hasSize(0));
        assertThat(sortedMapOf().isEmpty(), is(true));

        assertThat(sortedMapOf("a", "a").keySet(),
                   contains("a"));
        assertThat(sortedMapOf("a", "a", "b", "b").keySet(),
                   contains("a", "b"));
        assertThat(sortedMapOf("a", "a", "b", "b", "c", "c").keySet(),
                   contains("a", "b", "c"));
        assertThat(sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d").keySet(),
                   contains("a", "b", "c", "d"));
        assertThat(sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e").keySet(),
                   contains("a", "b", "c", "d", "e"));
        assertThat(sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e",
                               "f", "f").keySet(),
                   contains("a", "b", "c", "d", "e", "f"));
        assertThat(sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e",
                               "f", "f", "g", "g").keySet(),
                   contains("a", "b", "c", "d", "e", "f", "g"));
        assertThat(sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e",
                               "f", "f", "g", "g", "h", "h").keySet(),
                   contains("a", "b", "c", "d", "e", "f", "g", "h"));
        assertThat(sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e",
                               "f", "f", "g", "g", "h", "h", "i", "i").keySet(),
                   contains("a", "b", "c", "d", "e", "f", "g", "h", "i"));
        assertThat(sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e",
                               "f", "f", "g", "g", "h", "h", "i", "i", "j", "j").keySet(),
                   contains("a", "b", "c", "d", "e", "f", "g", "h", "i", "j"));
    }

    @Test
    public void testConstructor_CopyOf() {
        assertThat(UnmodifiableSortedMap.asSortedMap(new HashMap<>()),
                   is(sameInstance(sortedMapOf())));
        Map<String, String> tmp = sortedMapOf("a", "b");
        assertThat(UnmodifiableSortedMap.asSortedMap(tmp), is(sameInstance(tmp)));

        Map<String, String> other;
        other = new TreeMap<>(reverseOrder());
        other.putAll(tmp);
        assertThat(UnmodifiableSortedMap.asSortedMap(other), is(tmp));
        assertThat(UnmodifiableSortedMap.asSortedMap(other).comparator(), is(reverseOrder()));
        other = new HashMap<>(tmp);
        assertThat(UnmodifiableSortedMap.asSortedMap(other), is(tmp));
    }

    @Test
    public void testBuilder() {
        UnmodifiableSortedMap.Builder<String, String> builder = newBuilderNaturalOrder(5);
        builder.putAll();
        builder.putAll(mapOf());
        builder.putAll(sortedMapOf("A", "B"));
        builder.put("B", "NOT")
               .put("B", "C");
        Map<String, String> instance = builder.build();
        assertThat(builder.build(), is(sameInstance(instance)));
        assertThat(instance.size(), is(2));
        assertThat(instance, hasEntry("A", "B"));
        assertThat(instance, hasEntry("B", "C"));
        assertThat(instance.get("A"), is("B"));
        assertThat(instance.get("C"), is(nullValue()));

        builder.put("B", "MORE");

        assertThat(newBuilderNaturalOrder().build(), is(sameInstance(sortedMapOf())));
        assertThat(newBuilderNaturalOrder(5).build(), is(sameInstance(sortedMapOf())));
        assertThat(newBuilderReverseOrder().build(), is(sortedMapOf()));
        assertThat(newBuilderReverseOrder(5).build(), is(sortedMapOf()));

        Comparator<String> comparator = Comparator.comparing(String::length);
        assertThat(UnmodifiableSortedMap.<String, String>newBuilderOrderedBy(comparator)
                                        .put("a", "b").build().comparator(), is(comparator));
        assertThat(UnmodifiableSortedMap.<String, String>newBuilderOrderedBy(1, comparator)
                                        .put("a", "b").build().comparator(), is(comparator));
        assertThat(UnmodifiableSortedMap.<String, String>newBuilderNaturalOrder()
                                        .put("a", "b").build().comparator(), is(nullValue()));
        assertThat(UnmodifiableSortedMap.<String, String>newBuilderNaturalOrder(1)
                                        .put("a", "b").build().comparator(), is(nullValue()));
        assertThat(UnmodifiableSortedMap.<String, String>newBuilderReverseOrder()
                                        .put("a", "b").build().comparator(), is(reverseOrder()));
        assertThat(UnmodifiableSortedMap.<String, String>newBuilderReverseOrder(1)
                                        .put("a", "b").build().comparator(), is(reverseOrder()));

        for (int i = 0; i < 1050; ++i) {
            builder.put("foo", UUID.randomUUID().toString());
            builder.put("bar", UUID.randomUUID().toString());
            builder.put("baz", UUID.randomUUID().toString());
        }

        assertThat(builder.build().keySet(),
                   contains("A", "B", "bar", "baz", "foo"));
        assertThat(builder.build().size(), is(5));
    }

    @Test
    public void testSortedMap() {
        var map = sortedMapOf("b", "b", "d", "d", "f", "f");
        assertThat(map.keySet(), is(sameInstance(map.keySet())));
        assertThat(map.values(), is(sameInstance(map.values())));
        assertThat(sortedMapOf().keySet(), is(sameInstance(sortedSetOf())));
        assertThat(sortedMapOf().values(), is(sameInstance(listOf())));

        assertThat(map.subMap("a", "c"), is(new TreeMap<>(map).subMap("a", "c")));
        assertThat(map.subMap("b", "c"), is(new TreeMap<>(map).subMap("b", "c")));
        assertThat(map.subMap("b", "d"), is(new TreeMap<>(map).subMap("b", "d")));
        assertThat(map.subMap("f", "g"), is(new TreeMap<>(map).subMap("f", "g")));

        assertThat(map.subMap("a", "c").keySet(), contains("b"));
        assertThat(map.subMap("b", "c").keySet(), contains("b"));
        assertThat(map.subMap("b", "d").keySet(), contains("b"));
        assertThat(map.subMap("f", "g").keySet(), contains("f"));
        assertThat(map.subMap("g", "g"), is(sameInstance(sortedMapOf())));
        assertThat(map.subMap("a", "g"), is(sameInstance(map)));

        assertThat(map.tailMap("a"), is(sameInstance(map)));
        assertThat(map.tailMap("b"), is(sameInstance(map)));
        assertThat(map.tailMap("c").keySet(), contains("d", "f"));
        assertThat(map.tailMap("d").keySet(), contains("d", "f"));
        assertThat(map.tailMap("e").keySet(), contains("f"));
        assertThat(map.tailMap("f").keySet(), contains("f"));
        assertThat(map.tailMap("g"), is(sameInstance(sortedMapOf())));

        assertThat(map.headMap("a"), is(sameInstance(sortedMapOf())));
        assertThat(map.headMap("b"), is(sameInstance(sortedMapOf())));
        assertThat(map.headMap("c").keySet(), contains("b"));
        assertThat(map.headMap("d").keySet(), contains("b"));
        assertThat(map.headMap("e").keySet(), contains("b", "d"));
        assertThat(map.headMap("f").keySet(), contains("b", "d"));
        assertThat(map.headMap("g"), is(sameInstance(map)));

        assertThat(map.firstKey(), is("b"));
        assertThat(map.lastKey(), is("f"));
        assertThat(sortedMapOf("a", "foo", "b", "bar").firstKey(), is("a"));
        assertThat(sortedMapOf("a", "foo", "b", "bar").lastKey(), is("b"));
        try {
            fail("no exception: " + sortedMapOf().firstKey());
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("size == 0"));
        }
        try {
            fail("no exception: " + sortedMapOf().lastKey());
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("size == 0"));
        }

        assertThat(map.containsKey("a"), is(false));
        assertThat(map.containsKey("b"), is(true));
        assertThat(sortedMapOf().containsKey("c"), is(false));
    }

    @Test
    public void testNavigableMap() {
        var map = sortedMapOf("b", "b", "d", "d", "f", "f");

        assertThrows(UnsupportedOperationException.class, map::pollFirstEntry);
        assertThrows(UnsupportedOperationException.class, map::pollLastEntry);
        assertThrows(NoSuchElementException.class, () -> sortedMapOf().firstEntry());
        assertThrows(NoSuchElementException.class, () -> sortedMapOf().lastEntry());

        assertThat(map.navigableKeySet().asList(), is(List.of("b", "d", "f")));
        assertThat(map.descendingKeySet().asList(), is(List.of("f", "d", "b")));
        assertThat(map.descendingMap().keySet().asList(), is(List.of("f", "d", "b")));

        assertThat(map.firstEntry(), is(new AbstractMap.SimpleImmutableEntry<>("b", "b")));
        assertThat(map.firstKey(), is("b"));
        assertThat(map.lastEntry(), is(new AbstractMap.SimpleImmutableEntry<>("f", "f")));
        assertThat(map.lastKey(), is("f"));

        assertThat(map.floorEntry("a"), is(nullValue()));
        assertThat(map.floorEntry("b"), is(new AbstractMap.SimpleImmutableEntry<>("b", "b")));
        assertThat(map.lowerEntry("b"), is(nullValue()));
        assertThat(map.lowerEntry("c"), is(new AbstractMap.SimpleImmutableEntry<>("b", "b")));

        assertThat(map.higherEntry("e"), is(new AbstractMap.SimpleImmutableEntry<>("f", "f")));
        assertThat(map.higherEntry("f"), is(nullValue()));
        assertThat(map.ceilingEntry("f"), is(new AbstractMap.SimpleImmutableEntry<>("f", "f")));
        assertThat(map.ceilingEntry("g"), is(nullValue()));

        assertThat(map.floorKey("a"), is(nullValue()));
        assertThat(map.floorKey("b"), is("b"));
        assertThat(map.lowerKey("b"), is(nullValue()));
        assertThat(map.lowerKey("c"), is("b"));

        assertThat(map.higherKey("e"), is("f"));
        assertThat(map.higherKey("f"), is(nullValue()));
        assertThat(map.ceilingKey("f"), is("f"));
        assertThat(map.ceilingKey("g"), is(nullValue()));

    }

    @Test
    public void testWithEntry() {
        UnmodifiableSortedMap<String, Integer> from = sortedMapOf("foo", 1, "bar", 2);
        assertThat(from.withEntry("baz", 3), is(sortedMapOf("foo", 1, "bar", 2, "baz", 3)));
        assertThat(from.withEntry("foo", 3), is(sortedMapOf("foo", 3, "bar", 2)));
        assertThat(sortedMapOf().withEntry("foo", 1), is(sortedMapOf("foo", 1)));
        assertThat(UnmodifiableSortedMap.<String, Integer>newBuilderOrderedBy(Comparator.reverseOrder())
                                        .build()
                                        .withEntry("foo", 1),
                   is(sortedMapOf("foo", 1)));
    }

    @Test
    public void testWithEntries() {
        UnmodifiableSortedMap<String, Integer> from = sortedMapOf("foo", 1, "bar", 2);
        assertThat(from.withEntries(sortedMapOf("baz", 3)), is(sortedMapOf("foo", 1, "bar", 2, "baz", 3)));
        assertThat(from.withEntries(sortedMapOf("foo", 3)), is(sortedMapOf("foo", 3, "bar", 2)));
        assertThat(sortedMapOf().withEntries(from), is(sameInstance(from)));
        assertThat(from.withEntries(sortedMapOf()), is(sameInstance(from)));
        assertThat(UnmodifiableSortedMap.<String, Integer>newBuilderOrderedBy(Comparator.reverseOrder())
                                        .build()
                                        .withEntries(from),
                   is(from));
    }


    @Test
    public void testUnsupported_Map() {
        try {
            sortedMapOf().put(new Object(), new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            sortedMapOf().remove(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            sortedMapOf().putAll(new HashMap<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            sortedMapOf().clear();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }
}
