/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.stream.IntStream;

import static com.google.common.collect.Iterables.unmodifiableIterable;
import static java.util.Collections.emptyEnumeration;
import static java.util.Collections.emptyIterator;
import static java.util.Collections.enumeration;
import static net.morimekta.collect.UnmodifiableList.asList;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableList.toList;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.fail;

public class UnmodifiableListTest {
    @Test
    public void testSimpleList_Of() {
        assertThat(listOf(), is(sameInstance(listOf())));
        assertThat(listOf().equals(listOf().asList()), is(true));
        assertThat(listOf().asList(), is(sameInstance(listOf())));
        assertThat(listOf(), hasSize(0));
        assertThat(listOf().isEmpty(), is(true));
        assertThat(listOf().iterator().hasNext(), is(false));
        assertThat(listOf().contains(new Object()), is(false));
        assertThat(listOf().contains(null), is(false));
        assertThat(listOf("foo").equals(new HashSet<String>(){{add("foo");}}), is(false));
        assertThat(listOf("foo").equals(new ArrayList<String>()), is(false));
        assertThat(listOf("foo", "bar").equals(new ArrayList<String>(){{add("foo");add("foo");}}), is(false));
        assertThat(listOf("foo", "bar").equals(new ArrayList<String>(){{add("foo");add("bar");}}), is(true));
        assertThat(listOf("bar", "bar").equals(new LinkedList<String>(){{add("bar");add("bar");}}), is(true));
        assertThat(listOf("bar", "foo").equals(new LinkedList<String>(){{add("bar");add("bar");}}), is(false));
        assertThat(listOf("bar", "foo").toString(), is("[bar, foo]"));
        assertThat(listOf("bar", "foo").hashCode(), is(listOf("bar", "foo").hashCode()));
        assertThat(listOf("bar", "foo").hashCode(), is(not(listOf("bar", "bar").hashCode())));
        UnmodifiableList<String> tmp = listOf("bar", "foo");
        assertThat(tmp.hashCode(), is(tmp.hashCode()));
        assertThat(listOf("foo").contains("foo"), is(true));
        assertThat(listOf("foo"), contains("foo"));
        assertThat(listOf("foo", "bar"),
                   contains("foo", "bar"));
        assertThat(listOf("foo", "bar", "baz"),
                   contains("foo", "bar", "baz"));
        assertThat(listOf("foo", "bar", "baz", "boo"),
                   contains("foo", "bar", "baz", "boo"));
        assertThat(listOf("foo", "bar", "baz", "boo", "bin"),
                   contains("foo", "bar", "baz", "boo", "bin"));
        assertThat(listOf("foo", "bar", "baz", "boo", "bin", "ble"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble"));
        assertThat(listOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa"));
        assertThat(listOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee"));
        assertThat(listOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo"));
        assertThat(listOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo", "now"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo", "now"));
        assertThat(listOf("a", "b", "c").toArray(new String[0]),
                   is(new String[]{"a", "b", "c"}));
        assertThat(listOf().toArray(new String[]{}),
                   is(new String[]{}));
        assertThat(listOf().toArray(String[]::new),
                   is(new String[]{}));
        assertThat(listOf().toArray(),
                   is(new Object[]{}));
        assertThat(listOf("a", "b", "c").get(2), is("c"));
        assertThat(listOf("a", "c", "b", "c", "b").indexOf("c"), is(1));
        assertThat(listOf("a", "c", "b", "c", "b").indexOf("y"), is(-1));
        assertThat(listOf("a", "c", "b", "c", "b").lastIndexOf("c"), is(3));
        assertThat(listOf("a", "c", "b", "c", "b").lastIndexOf("y"), is(-1));
        assertThat(listOf("a", "c", "b", "c", "b").listIterator().next(), is("a"));
        assertThat(listOf("a", "c", "b", "c", "b").listIterator(2).next(), is("b"));
        assertThat(listOf("a", "b").subList(0, 1), is(listOf("a")));
        assertThat(listOf("a", "b").subList(1, 2), is(listOf("b")));
        tmp = listOf("a", "c", "b", "c", "b");
        assertThat(tmp.subList(0, 5), is(sameInstance(tmp)));
        assertThat(tmp.subList(2, 3), is(tmp.subList(2, 3)));
    }

    @Test
    public void testConstructor_CopyOf() {
        assertThat(asList(),
                   is(sameInstance(listOf())));
        assertThat(asList(emptyEnumeration()),
                   is(sameInstance(listOf())));
        assertThat(asList(emptyIterator()),
                   is(sameInstance(listOf())));
        assertThat(asList(List.of()),
                   is(sameInstance(listOf())));
        assertThat(asList(Set.of()),
                   is(sameInstance(listOf())));
        assertThat(asList((Iterable<?>) setOf()),
                   is(sameInstance(listOf())));
        assertThat(asList((Iterable<?>) Set.of()),
                   is(sameInstance(listOf())));
        assertThat(asList(setOf()),
                   is(sameInstance(listOf())));
        assertThat(asList(unmodifiableIterable(new LinkedList<>())),
                   is(sameInstance(listOf())));

        assertThat(asList("a", "b", "c"),
                   is(listOf("a", "b", "c")));
        assertThat(asList("a", "b", "c"),
                   is(not(listOf("a", "c", "b"))));
        assertThat(asList(enumeration(setOf("a", "b", "c"))),
                   is(listOf("a", "b", "c")));
        assertThat(asList(listOf("a", "b", "c")),
                   is(listOf("a", "b", "c")));
        assertThat(asList(Arrays.asList("a", "b", "c")),
                   is(listOf("a", "b", "c")));
        assertThat(asList(listOf("a", "b", "c").iterator()),
                   is(listOf("a", "b", "c")));
    }

    @Test
    public void testCollector() {
        assertThat(asList("a", "b", "c"), is(listOf("a", "b", "c")));
        UnmodifiableList<Integer> collected = IntStream.range(2, 5).boxed().collect(toList());
        assertThat(collected, is(listOf(2, 3, 4)));
        collected = IntStream.range(0, 4096).parallel().boxed().collect(toList());
        assertThat(collected, hasSize(4096));
    }

    @Test
    @SuppressWarnings("unchecked,rawtypes")
    public void testBuilder() {
        UnmodifiableList.Builder<String> builder = UnmodifiableList.newBuilder(5);
        assertThat(builder.addAll(Arrays.asList("a", "c")), is(sameInstance(builder)));
        assertThat(builder.add("b"), is(sameInstance(builder)));
        assertThat(builder.addAll(), is(sameInstance(builder)));
        assertThat(builder.addAll("e", "f"), is(sameInstance(builder)));
        assertThat(builder.addAll(unmodifiableIterable(listOf("g"))),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(listOf("b", "d").iterator()),
                   is(sameInstance(builder)));
        assertThat(builder.addAll((Iterable<String>) (Iterable) listOf()), is(sameInstance(builder)));
        assertThat(builder.addAll(enumeration(listOf("i"))),
                   is(sameInstance(builder)));
        assertThat(builder.build(),
                   contains("a", "c", "b", "e", "f", "g", "b", "d", "i"));
    }

    @Test
    public void testSubList() {
        List<String> test = listOf("a", "b", "c");
        assertThat(test.subList(0, 3), is(sameInstance(test)));
        assertThat(test.subList(1, 1), is(sameInstance(listOf())));
        assertThat(test.subList(0, 1), contains("a"));
        assertThat(test.subList(2, 3), contains("c"));
        assertThat(test.subList(0, 1).toString(), is("[a]"));
        assertThat(test.subList(2, 3).toString(), is("[c]"));
    }

    @Test
    public void testCollection_Methods() {
        assertThat(listOf().contains("foo"), is(false));
        assertThat(listOf("foo").contains("foo"), is(true));
        assertThat(listOf("foo").contains("bar"), is(false));
        assertThat(listOf("foo").containsAll(listOf("foo", "bar")), is(false));
        assertThat(listOf("foo", "bar").containsAll(listOf("foo", "bar")), is(true));
        assertThat(listOf().containsAll(listOf("foo", "bar")), is(false));
        assertThat(listOf().containsAll(listOf()), is(true));
    }

    @Test
    public void testFiltered() {
        UnmodifiableList<String> ab = listOf("a", "b");
        assertThat(listOf().filtered("a"::equals), is(listOf()));
        assertThat(ab.filtered("a"::equals), is(listOf("a")));
        assertThat(ab.filtered(s -> !"".equals(s)), is(sameInstance(ab)));
    }

    @Test
    public void testCollection_ListIterator() {
        ListIterator<String> it = listOf("a", "b", "c").listIterator(2);
        assertThat(it.hasPrevious(), is(true));
        assertThat(it.previousIndex(), is(1));
        assertThat(it.nextIndex(), is(2));
        assertThat(it.next(), is("c"));
        assertThat(it.hasNext(), is(false));
        try {
            it.next();
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("Index 3 out of bounds for length 3"));
        }
        assertThat(it.previous(), is("c"));
        assertThat(it.previous(), is("b"));
        assertThat(it.previous(), is("a"));
        assertThat(it.hasPrevious(), is(false));
        try {
            it.previous();
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("Index -1 out of bounds for length 3"));
        }
    }

    @Test
    public void testBadInput() {
        try {
            listOf("foo").get(1);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), anyOf(
                    is("Index 1 out of bounds for length 1"),
                    // Case for java 8
                    is("1")));
        }
        try {
            listOf("foo").listIterator(2);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("ListIterator index 2 out of bounds for length 1"));
        }
        try {
            listOf("foo", "bar", "baz").subList(-1, 2);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("Start index -1 out of bounds for length 3"));
        }
        try {
            listOf("foo", "bar", "baz").subList(2, 1);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("End index 1 out of bounds for length 3, start 2"));
        }
        try {
            listOf("foo", "bar", "baz").subList(1, 5);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("End index 5 out of bounds for length 3, start 1"));
        }
        try {
            listOf(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("null item at pos 0"));
        }
        try {
            asList((String[]) null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("null array"));
        }
        try {
            asList((Collection<?>) null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("null collection"));
        }
        try {
            asList(new ArrayList<>(){{add(null);}});
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("null item at pos 0"));
        }
    }

    @Test
    public void testConstructor_BadInput() {
        Object[] array = new Object[1];
        array[0] = new Object();
        try {
            new UnmodifiableList<>(-1, 0, array);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("offset: -1 length: 0"));
        }
        try {
            new UnmodifiableList<>(0, -1, array);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("offset: 0 length: -1"));
        }
        try {
            new UnmodifiableList<>(-1, -1, array);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("offset: -1 length: -1"));
        }
        try {
            new UnmodifiableList<>(0, 2, array);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("offset: 0 + length: 2 > array: 1"));
        }
    }

    @Test
    public void testUnsupported_List() {
        try {
            listOf().add(0, new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().remove(0);
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().addAll(0, new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().set(0, new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().add(0, new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().remove(0);
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }

    @Test
    public void testUnsupported_ListIterator() {
        try {
            listOf().listIterator().remove();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().listIterator().set(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().listIterator().add(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }
}
