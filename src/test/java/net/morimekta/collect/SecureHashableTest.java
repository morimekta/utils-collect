/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static net.morimekta.collect.SecureHashable.secureHash;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.collect.util.Binary.fromHexString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class SecureHashableTest {
    public static Stream<Arguments> dataSecureHashCode_Objects() {
        return Stream.of(
                // strings
                arguments("",     6965299255647741657L),
                arguments("a",    2178581036532865693L),
                arguments("aa",   2912620372064619465L),
                arguments("ab",  -4823058556094743810L),
                arguments("ba",  -7562910290555268928L),
                arguments("bb",   3148154854994919413L),
                arguments("aaa", -7072128921380580883L),
                // lists
                arguments(listOf(),     -1466376081728027815L),
                arguments(listOf(1),    -3342334528179817803L),
                arguments(listOf(1, 1),  2438898382148634297L),
                arguments(listOf(1, 2), -5585550360657720396L),
                arguments(listOf(2, 1), -8878486560186571978L),
                // sets
                arguments(setOf(),       965024946503620458L),
                arguments(setOf(1),      713283505352655249L),
                arguments(setOf(2),    -7311165237453699444L),
                arguments(setOf(1, 2), -7562906678604664653L),
                arguments(setOf(2, 1), -7562906678604664653L),
                // maps
                arguments(mapOf(),      3722818190163320554L),
                arguments(mapOf(1, 2), -6951778980966287457L),
                arguments(mapOf(2, 1),   202351351119937239L),
                arguments(mapOf(1, 2, 3, 4),  1392662095154824666L),
                arguments(mapOf(1, 4, 3, 2), -5675765559938858098L));
    }

    @ParameterizedTest
    @MethodSource("dataSecureHashCode_Objects")
    public void testSecureHashCode_Objects(Object o, long hash) {
        assertThat(secureHash(o), is(hash));
    }

    public static Stream<Arguments> dataSecureHashCode_Binary() {
        return Stream.of(
                arguments("",    8812079937318930253L),
                arguments("01",  7876671002614923053L),
                arguments("02", -1899957847228259366L),
                arguments("03",  6770157376638109831L),
                arguments("04", -3006471473205072588L),
                arguments("2255",  -474668358771332032L),
                arguments("2256",  8195446865095037165L),
                arguments("2257", -1581181984748145254L),
                arguments("2258",  7088933239118223943L),
                arguments("3242",  4698758761032593497L),
                arguments("3342", -1975962784741052376L),
                arguments("3442", -8650684330514698249L),
                arguments("3542",  3121338197421207494L));
    }

    @ParameterizedTest
    @MethodSource("dataSecureHashCode_Binary")
    public void testSecureHashCode_Binary(String hex, long hash) {
        var binary = fromHexString(hex);
        assertThat(binary.secureHashCode(), is(hash));
        assertThat(secureHash(binary), is(hash));
        assertThat(secureHash((Object) binary.get()), is(hash));
    }
}
