/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect.util;


import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.util.ExtraCollectors.groupingBy;
import static net.morimekta.collect.util.ExtraCollectors.groupingByAll;
import static net.morimekta.collect.util.ExtraCollectors.inBatchesOf;
import static net.morimekta.collect.util.ExtraCollectors.inNumBatches;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Tests for extra collectors.
 */
public class ExtraCollectorsTest {
    @Test
    public void testInBatchesOf() {
        List<Integer> sizes = IntStream.range(0, 10000)
                                       .mapToObj(String::valueOf)
                                       .collect(inBatchesOf(1037))
                                       .stream()
                                       .map(List::size)
                                       .collect(Collectors.toList());
        assertThat(sizes, is(equalTo(listOf(1037, 1037, 1037, 1037, 1037, 1037, 1037, 1037, 1037, 667))));

        sizes = IntStream.range(0, 10000)
                         .parallel()
                         .boxed()
                         .collect(inBatchesOf(1037))
                         .stream()
                         .map(List::size)
                         .collect(Collectors.toList());

        assertThat(sizes, is(equalTo(listOf(1037, 1037, 1037, 1037, 1037, 1037, 1037, 1037, 1037, 667))));

        sizes = IntStream.range(0, 100_000)
                         .parallel()
                         .boxed()
                         .filter(i -> i > 100_000)
                         .collect(inBatchesOf(1037))
                         .stream()
                         .map(List::size)
                         .collect(Collectors.toList());

        assertThat(sizes, is(equalTo(listOf())));
    }

    @Test
    public void testInNumBatches() {
        List<Integer> sizes = IntStream.range(0, 10000)
                                       .mapToObj(String::valueOf)
                                       .collect(inNumBatches(9))
                                       .stream()
                                       .map(List::size)
                                       .collect(Collectors.toList());
        assertThat(sizes, is(equalTo(listOf(1112, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 1111))));

        sizes = IntStream.range(0, 10000)
                         .parallel()
                         .boxed()
                         .collect(inNumBatches(11))
                         .stream()
                         .map(List::size)
                         .collect(Collectors.toList());

        assertThat(sizes, is(equalTo(listOf(910, 909, 909, 909, 909, 909, 909, 909, 909, 909, 909))));

        sizes = IntStream.range(0, 5)
                .parallel()
                .boxed()
                .collect(inNumBatches(10))
                .stream()
                .map(List::size)
                .collect(Collectors.toList());
        assertThat(sizes, is(equalTo(listOf(1, 1, 1, 1, 1))));
    }

    @Test
    public void testGroupingBy() {
        assertThat(listOf("Donald Trump",
                          "Barack Obama",
                          "George W. Bush",
                          "Bill Clinton",
                          "George H. W. Bush")
                           .stream()
                           .parallel()
                           .collect(groupingBy(k -> k.replaceAll(".* ", ""))),
                   CoreMatchers.is(mapOf("Trump", listOf("Donald Trump"),
                                         "Obama", listOf("Barack Obama"),
                                         "Clinton", listOf("Bill Clinton"),
                                         "Bush", listOf("George W. Bush", "George H. W. Bush"))));
    }

    @Test
    public void testGroupingByAll() {
        assertThat(listOf("Donald Trump",
                          "Barack Obama",
                          "George W. Bush",
                          "Bill Clinton",
                          "George H. W. Bush")
                           .stream()
                           .parallel()
                           .collect(groupingByAll(k -> listOf(
                                   k.replaceAll(".* ", ""),
                                   k.replaceAll(" .*", "")))),
                   CoreMatchers.is(mapOf(
                           "Donald", listOf("Donald Trump"),
                           "Trump", listOf("Donald Trump"),
                           "Barack", listOf("Barack Obama"),
                           "Obama", listOf("Barack Obama"),
                           "Clinton", listOf("Bill Clinton"),
                           "Bill", listOf("Bill Clinton"),
                           "George", listOf("George W. Bush", "George H. W. Bush"),
                           "Bush", listOf("George W. Bush", "George H. W. Bush"))));
    }

}
