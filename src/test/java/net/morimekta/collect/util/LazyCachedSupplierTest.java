/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect.util;

import com.google.common.util.concurrent.UncheckedExecutionException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.BooleanSupplier;
import java.util.function.IntSupplier;
import java.util.function.LongSupplier;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static net.morimekta.collect.util.LazyCachedBoolean.lazyBoolean;
import static net.morimekta.collect.util.LazyCachedInteger.lazyInt;
import static net.morimekta.collect.util.LazyCachedLong.lazyLong;
import static net.morimekta.collect.util.LazyCachedSupplier.lazyCache;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LazyCachedSupplierTest {
    ExecutorService executor;

    @BeforeEach
    public void setUp() {
        executor = Executors.newFixedThreadPool(10);
    }

    @AfterEach
    public void tearDown() {
        executor.shutdownNow();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testCachedSupplier() throws InterruptedException {
        for (int i = 0; i < 100; ++i) {
            Supplier<String> origin = (Supplier<String>) mock(Supplier.class);
            when(origin.get()).thenReturn("" + i);

            LazyCachedSupplier<String> cached = lazyCache(origin);
            List<Future<String>> futures =
                    executor.invokeAll(List.of(
                            cached::get,
                            cached::get,
                            () -> cached.reload().get(),
                            cached::get,
                            cached::get,
                            cached::get,
                            cached::get,
                            cached::get,
                            cached::get,
                            cached::get));
            List<String> results = futures
                    .stream()
                    .map((Future<String> f) -> {
                        try {
                            return f.get();
                        } catch (Exception e) {
                            throw new UncheckedExecutionException(e);
                        }
                    })
                    .collect(Collectors.toList());
            for (String s : results) {
                assertThat(s, is("" + i));
            }
            verify(origin, times(2)).get();
        }
    }

    @Test
    public void testCachedBoolean() throws InterruptedException {
        for (int i = 0; i < 100; ++i) {
            BooleanSupplier origin = mock(BooleanSupplier.class);
            when(origin.getAsBoolean()).thenReturn(true);

            LazyCachedBoolean cached = lazyBoolean(origin);
            List<Future<Boolean>> futures =
                    executor.invokeAll(List.of(
                            cached::getAsBoolean,
                            cached::getAsBoolean,
                            () -> cached.reload().getAsBoolean(),
                            cached::getAsBoolean,
                            cached::getAsBoolean,
                            cached::getAsBoolean,
                            cached::getAsBoolean,
                            cached::getAsBoolean,
                            cached::getAsBoolean,
                            cached::getAsBoolean));
            List<Boolean> results = futures
                    .stream()
                    .map((Future<Boolean> f) -> {
                        try {
                            return f.get();
                        } catch (Exception e) {
                            throw new UncheckedExecutionException(e);
                        }
                    })
                    .collect(Collectors.toList());
            for (Boolean s : results) {
                assertThat(s, is(true));
            }
            verify(origin, times(2)).getAsBoolean();
        }
    }

    @Test
    public void testCachedInteger() throws InterruptedException {
        for (int i = 0; i < 100; ++i) {
            IntSupplier origin = mock(IntSupplier.class);
            when(origin.getAsInt()).thenReturn(i);

            LazyCachedInteger cached = lazyInt(origin);
            List<Future<Integer>> futures =
                    executor.invokeAll(List.of(
                            cached::getAsInt,
                            cached::getAsInt,
                            () -> cached.reload().getAsInt(),
                            cached::getAsInt,
                            cached::getAsInt,
                            cached::getAsInt,
                            cached::getAsInt,
                            cached::getAsInt,
                            cached::getAsInt,
                            cached::getAsInt));
            List<Integer> results = futures
                    .stream()
                    .map((Future<Integer> f) -> {
                        try {
                            return f.get();
                        } catch (Exception e) {
                            throw new UncheckedExecutionException(e);
                        }
                    })
                    .collect(Collectors.toList());
            for (Integer s : results) {
                assertThat(s, is(i));
            }
            verify(origin, times(2)).getAsInt();
        }
    }

    @Test
    public void testCachedLong() throws InterruptedException {
        for (long i = 0; i < 100; ++i) {
            LongSupplier origin = mock(LongSupplier.class);
            when(origin.getAsLong()).thenReturn(i);

            LazyCachedLong cached = lazyLong(origin);
            List<Future<Long>> futures =
                    executor.invokeAll(List.of(
                            cached::getAsLong,
                            cached::getAsLong,
                            () -> cached.reload().getAsLong(),
                            cached::getAsLong,
                            cached::getAsLong,
                            cached::getAsLong,
                            cached::getAsLong,
                            cached::getAsLong,
                            cached::getAsLong,
                            cached::getAsLong));
            List<Long> results = futures
                    .stream()
                    .map((Future<Long> f) -> {
                        try {
                            return f.get();
                        } catch (Exception e) {
                            throw new UncheckedExecutionException(e);
                        }
                    })
                    .collect(Collectors.toList());
            for (Long s : results) {
                assertThat(s, is(i));
            }
            verify(origin, times(2)).getAsLong();
        }
    }
}
