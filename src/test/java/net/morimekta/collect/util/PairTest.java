/*
 * Copyright (c) 2016, Stein Eldar johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect.util;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class PairTest {
    @Test
    public void testGetters() {
        Pair<Integer, String> a1 = Pair.pairOf(4, "a");
        Pair<Integer, String> a2 = Pair.pairOf(6, "a");
        Pair<Integer, String> b1 = Pair.pairOf(4, "b");
        Pair<Integer, String> b2 = Pair.pairOf(6, "b");

        List<Pair<Integer, String>> list = new ArrayList<>();
        Collections.addAll(list, a1, a2, b1, b2);

        list.sort(Comparator.comparing(Pair::getFirst));

        assertThat(list, is(listOf(a1, b1, a2, b2)));

        list.sort(Comparator.comparing(Pair::getSecond));

        assertThat(list, is(listOf(a1, a2, b1, b2)));
    }

    @Test
    public void testEquals() {
        Pair<Integer, String> a = Pair.pairOf(4, "a");
        Pair<Integer, String> b1 = Pair.pairOf(4, "b");
        Pair<Integer, String> b2 = Pair.pairOf(4, "b");

        assertThat(a, is(not(b1)));
        assertThat(b1, is(b2));

        assertThat(a.equals(a), is(true));
        assertThat(a.equals(null), is(false));
        assertThat(a.equals(new Object()), is(false));
    }

    @Test
    public void testHashCode() {
        Pair<Integer, String> a = Pair.pairOf(4, "a");
        Pair<Integer, String> b1 = Pair.pairOf(4, "b");
        Pair<Integer, String> b2 = Pair.pairOf(4, "b");
        Pair<UUID, String> c1 = Pair.pairOf(null, "c");
        Pair<String, UUID> c2 = Pair.pairOf("c", null);

        assertThat(a.hashCode(), is(not(b1.hashCode())));
        assertThat(b1.hashCode(), is(b2.hashCode()));
        assertThat(c1.hashCode(), is(not(c2.hashCode())));
    }

    @Test
    public void testToString() {
        Pair<Integer, String> a = Pair.pairOf(4, "a");
        Pair<Integer, String> b = Pair.pairOf(4, "b");
        Pair<UUID, String> c = Pair.pairOf(null, "c");

        assertThat(a.toString(), is("(4,a)"));
        assertThat(b.toString(), is("(4,b)"));
        assertThat(c.toString(), is("(null,c)"));
    }
}
