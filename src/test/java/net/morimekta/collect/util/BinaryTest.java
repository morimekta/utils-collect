/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.BitSet;
import java.util.TreeSet;

import static java.nio.charset.StandardCharsets.UTF_16BE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Created by SteinEldar on 24.12.2015.
 */
public class BinaryTest {
    private byte[] a1;
    private byte[] a2;
    private byte[] b1;
    private byte[] b2;
    private byte[] c1;
    private byte[] c2;

    @BeforeEach
    public void setUp() {
        a1 = new byte[]{'a', 'b', 'c'};
        a2 = new byte[]{'a', 'b', 'c'};
        b1 = new byte[]{'a', 'b', 'd'};
        b2 = new byte[]{'a', 'b', 'd'};
        c1 = new byte[]{'a', 'b', 'c', 'd'};
        c2 = new byte[]{'a', 'b', 'c', 'd'};
    }

    @Test
    public void testConstructor() {
        try {
            fail("no exception: " + new Binary(null));
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("bytes == null"));
        }
        try {
            fail("no exception: " + new Binary(null, 0, 2));
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("bytes == null"));
        }
        try {
            fail("no exception: " + new Binary(c1, -1, 1));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("offset -1 < 0"));
        }
        try {
            fail("no exception: " + new Binary(c1, 1, -1));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("length -1 < 0"));
        }
        try {
            fail("no exception: " + new Binary(c1, 3, 2));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("End after end of bytes: 3 + 2 > 4"));
        }
    }

    @Test
    public void testWrap() {
        // Wrap refers to the same array, so they will be modified in sync
        // with the original array.
        Binary a = Binary.wrap(a1);
        Binary b = Binary.wrap(a1);

        assertThat(a, is(b));
        assertThat(a.toHexString(), is("616263"));

        a1[2] = 'e';

        assertThat(a, is(b));
        assertThat(a.toHexString(), is("616265"));
    }

    @Test
    public void testSlice() {
        Binary a = Binary.wrap(c1);
        Binary b = Binary.wrap(c2);
        assertThat(a.slice(1, 2).toHexString(), is("62"));
        assertThat(a.slice(1, 2), is(b.slice(1, 2)));
        assertThat(a.slice(1, 2), is(not(b.slice(2, 3))));
        assertThat(a.slice(2).get(), is(new byte[]{'c', 'd'}));
        assertThat(Binary.wrap(a1).slice(1), is(b.slice(1, 3)));
        assertThat(Binary.wrap(b1).slice(0, 2), is(b.slice(0, 2)));
        assertThat(a.slice(2).toBase64(), is("Y2Q"));
        assertThat(a.slice(0), is(sameInstance(a)));
        assertThat(a.slice(0, 4), is(sameInstance(a)));
        assertThat(a.slice(0, 0), is(sameInstance(Binary.empty())));

        try {
            fail("no exception: " + a.slice(-1, 0));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("start -1 < 0"));
        }
        try {
            fail("no exception: " + a.slice(2, 1));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("start 2 > end 1"));
        }
        try {
            fail("no exception: " + a.slice(1, 5));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("end 5 > length 4"));
        }
    }

    @Test
    public void testEmpty() {
        // Copy makes an in-memory copy, and will thus be unaffected by
        // modifications to the original.
        Binary empty = Binary.empty();

        assertThat(empty, is(sameInstance(Binary.empty())));
        assertThat(Binary.wrap(new byte[]{}), is(sameInstance(Binary.empty())));
        assertThat(Binary.copy(new byte[]{}), is(sameInstance(Binary.empty())));
        assertThat(Binary.copy(new byte[]{0, 1, 2, 3, 4}, 5, 0), is(sameInstance(Binary.empty())));

        // Not modified.
        assertThat(empty.toHexString(), is(""));
    }

    @Test
    public void testCopy() {
        // Copy makes an in-memory copy, and will thus be unaffected by
        // modifications to the original.
        Binary a = Binary.copy(a1);
        Binary c = Binary.copy(c1, 1, 2);

        assertThat(a.toHexString(), is("616263"));
        assertThat(c.toHexString(), is("6263"));

        a1[2] = 'e';
        c1[2] = 'f';

        // Not modified.
        assertThat(a.toHexString(), is("616263"));
        assertThat(c.toHexString(), is("6263"));
    }

    @Test
    public void testGet() {
        // Gets a copy of the array, so modifications to it will not affect
        // the contained array.
        Binary c = Binary.wrap(c1);
        assertThat(c.toHexString(), is("61626364"));

        byte[] copy = c.get();

        assertArrayEquals(c1, copy);

        copy[1] = 'B';

        assertThat(Arrays.equals(copy, c1), is(false));
        assertThat(c.toHexString(), is("61626364"));

        Arrays.fill(copy, (byte) 0);

        c.get(copy);
        assertArrayEquals(c1, copy);

        byte[] sh = new byte[3];
        c.get(sh);
        assertArrayEquals(a1, sh);
    }

    @Test
    public void testBase64() {
        String b64 = Base64.getEncoder().encodeToString(a1);

        assertThat(Binary.wrap(a1).toBase64(), is(b64));
        assertThat(Binary.wrap(a2), is(Binary.fromBase64(b64)));
        assertThat(Binary.fromBase64(""), is(sameInstance(Binary.empty())));
    }

    @Test
    public void testBitSet() {
        BitSet set = new BitSet();
        set.set(2);
        set.set(5);

        Binary binary = Binary.fromBitSet(set);

        assertThat(binary, is(Binary.wrap(new byte[]{0x24})));
        assertThat(binary.toBitSet(), is(set));
    }

    @Test
    public void testHexString() {
        Binary a = Binary.wrap(a1);
        assertThat(a.toHexString(), is("616263"));
        assertThat(a, is(Binary.fromHexString("616263")));
        assertThat(Binary.empty().toHexString(), is(""));
        assertThat(Binary.fromHexString(""), is(sameInstance(Binary.empty())));

        try {
            Binary.fromHexString("a");
            fail("No exception on bad input");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Illegal hex string length: 1"));
        }
    }

    @Test
    public void testEncodedString() {
        String str = "foo%¼€";
        Binary data = Binary.encodeFromString(str, UTF_16BE);
        assertThat(data.length(), is(12));
        assertThat(data.decodeToString(UTF_16BE), is(str));
    }

    @Test
    public void testGetByteBuffer() {
        // Just test that the byte buffer contains a copy, and not the
        // original array.
        Binary a = Binary.wrap(a1);
        ByteBuffer b = a.getByteBuffer();
        assertThat(a1, is(not(sameInstance(b.array()))));
        assertThat(Binary.fromByteBuffer(b), is(a));
        ByteBuffer e = Binary.empty().getByteBuffer();
        assertThat(Binary.fromByteBuffer(e), is(sameInstance(Binary.empty())));
    }

    @Test
    public void testGetInputStream() throws IOException {
        Binary a = Binary.wrap(a1);
        BufferedInputStream bis = new BufferedInputStream(a.getInputStream());
        byte[] allBytes = bis.readAllBytes();
        assertThat(allBytes, is(equalTo(a1)));
    }

    @Test
    public void testIoStreams() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        Binary c = Binary.wrap(c1);
        assertThat(c.write(baos), is(c1.length));

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        Binary o = Binary.read(bais, baos.size());

        assertThat(c, is(o));
        assertThat(c.length(), is(o.length()));

        bais = new ByteArrayInputStream(baos.toByteArray());
        Binary p = Binary.read(bais);
        assertThat(c, is(p));

        Binary instance = Binary.read(InputStream.nullInputStream());
        assertThat(instance, is(sameInstance(Binary.empty())));

        instance = Binary.read(InputStream.nullInputStream(), 0);
        assertThat(instance, is(sameInstance(Binary.empty())));
    }

    @Test
    public void testReadNotEnough() {
        ByteArrayInputStream bais = new ByteArrayInputStream(c1);

        try {
            Binary.read(bais, c1.length + 1);
            fail("No exception on too short input array");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("End of stream before complete buffer read."));
        }
    }

    @Test
    public void testHashCode() {
        assertThat(Binary.wrap(a1).hashCode(),
                   is(Binary.wrap(a2).hashCode()));
        assertThat(Binary.wrap(b1).hashCode(),
                   is(Binary.wrap(b2).hashCode()));
        assertThat(Binary.wrap(c1).hashCode(),
                   is(Binary.wrap(c2).hashCode()));

        assertThat(Binary.wrap(b1).hashCode(),
                   is(not(Binary.wrap(a2).hashCode())));
        assertThat(Binary.wrap(c1).hashCode(),
                   is(not(Binary.wrap(b2).hashCode())));
        assertThat(Binary.wrap(a1).hashCode(),
                   is(not(Binary.wrap(c2).hashCode())));
    }

    @Test
    public void testEquals() {
        assertThat(Binary.wrap(a1), is(Binary.wrap(a2)));
        assertThat(Binary.wrap(b1), is(Binary.wrap(b2)));
        assertThat(Binary.wrap(c1), is(Binary.wrap(c2)));

        assertThat(Binary.wrap(b1), is(not(Binary.wrap(a2))));
        assertThat(Binary.wrap(c1), is(not(Binary.wrap(b2))));
        assertThat(Binary.wrap(a1), is(not(Binary.wrap(c2))));

        Binary a = Binary.wrap(a1);
        assertThat(a, is(a));
        assertThat(a, is(not(nullValue())));
        assertThat(a, is(not(new Object())));
    }

    @Test
    public void testCompareTo() {
        TreeSet<Binary> set = new TreeSet<>();
        set.add(Binary.wrap(a1));
        set.add(Binary.wrap(a2));
        set.add(Binary.wrap(b1));
        set.add(Binary.wrap(b2));
        set.add(Binary.wrap(c1));
        set.add(Binary.wrap(c2));

        assertThat(set, hasSize(3));
        ArrayList<Binary> list = new ArrayList<>(set);
        assertThat(list.get(0), is(Binary.wrap(a1)));
        assertThat(list.get(1), is(Binary.wrap(c1)));
        assertThat(list.get(2), is(Binary.wrap(b1)));

        assertThat(Binary.wrap(a1).compareTo(Binary.wrap(c1)), is(-1));
        assertThat(Binary.wrap(c1).compareTo(Binary.wrap(a1)), is(1));
    }

    @Test
    public void testToString() {
        assertThat(Binary.wrap(a1).toString(), is("binary(616263)"));
    }
}
