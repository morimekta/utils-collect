/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect.util;


import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.collect.UnmodifiableSortedSet;
import org.junit.jupiter.api.Test;

import static java.util.Comparator.reverseOrder;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.collect.UnmodifiableSortedSet.sortedSetOf;
import static net.morimekta.collect.util.Pair.pairOf;
import static net.morimekta.collect.util.SetOperations.difference;
import static net.morimekta.collect.util.SetOperations.disjoint;
import static net.morimekta.collect.util.SetOperations.intersect;
import static net.morimekta.collect.util.SetOperations.product;
import static net.morimekta.collect.util.SetOperations.subset;
import static net.morimekta.collect.util.SetOperations.subtract;
import static net.morimekta.collect.util.SetOperations.union;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;

public class SetOperationsTest {
    @Test
    public void testUnion() {
        assertThat(union(), is(setOf()));
        assertThat(union(setOf(2, 4, 6)), is(setOf(2, 4, 6)));
        assertThat(union(setOf(1, 2, 3), setOf(2, 3, 4), setOf(3, 4, 5)),
                   is(setOf(1, 2, 3, 4, 5)));
        assertThat(union(sortedSetOf(5, 4, 3), setOf(1, 2, 3)),
                   is(sortedSetOf(1, 2, 3, 4, 5)));
        assertThat(union(sortedSetOf(5, 4, 3), setOf(1, 2, 3)),
                   is(instanceOf(UnmodifiableSortedSet.class)));
    }

    @Test
    public void testIntersect() {
        assertThat(intersect(setOf(1, 2, 3), setOf(2, 3, 4)),
                   is(instanceOf(UnmodifiableSet.class)));
        assertThat(intersect(sortedSetOf(1, 2, 3), setOf(2, 3, 4)),
                   is(instanceOf(UnmodifiableSortedSet.class)));

        assertThat(intersect(setOf(1, 2)), is(setOf(1, 2)));
        assertThat(intersect(setOf(), setOf(1, 2, 3)), is(setOf()));
        assertThat(intersect(setOf(1, 2, 3), setOf(2, 3, 4)), is(setOf(2, 3)));
        assertThat(intersect(setOf(1, 2, 3), setOf(4, 5, 6)), is(setOf()));

        assertThat(intersect(sortedSetOf(1, 2)), is(setOf(1, 2)));
        assertThat(intersect(sortedSetOf(), setOf(1, 2, 3)), is(setOf()));
        assertThat(intersect(sortedSetOf(1, 2, 3), setOf(2, 3, 4)), is(setOf(2, 3)));
        assertThat(intersect(sortedSetOf(1, 2, 3), setOf(4, 5, 6)), is(setOf()));
    }

    @Test
    public void testSubtract() {
        assertThat(subtract(setOf(1, 2)), is(instanceOf(UnmodifiableSet.class)));
        assertThat(subtract(sortedSetOf(1, 2)), is(instanceOf(UnmodifiableSortedSet.class)));

        assertThat(subtract(setOf()), is(setOf()));
        assertThat(subtract(setOf(1, 2, 3)), is(setOf(1, 2, 3)));
        assertThat(subtract(setOf(1, 2, 3), setOf(3, 4, 5)), is(setOf(1, 2)));
        assertThat(subtract(sortedSetOf()), is(sortedSetOf()));
        assertThat(subtract(sortedSetOf(1, 2, 3)), is(sortedSetOf(1, 2, 3)));
        assertThat(subtract(sortedSetOf(1, 2, 3), setOf(3, 4, 5)), is(sortedSetOf(1, 2)));
    }

    @Test
    public void testDifference() {
        assertThat(difference(sortedSetOf(), setOf()), is(instanceOf(UnmodifiableSortedSet.class)));
        assertThat(difference(setOf(), sortedSetOf()), is(instanceOf(UnmodifiableSet.class)));

        assertThat(difference(sortedSetOf(1, 2), setOf()), is(setOf(1, 2)));
        assertThat(difference(sortedSetOf(), setOf(1, 2)), is(setOf(1, 2)));
        assertThat(difference(setOf(1, 2), setOf()), is(setOf(1, 2)));
        assertThat(difference(setOf(), setOf(1, 2)), is(setOf(1, 2)));

        assertThat(difference(setOf(1, 2, 3), setOf(2, 3, 4)),
                   is(setOf(1, 4)));
        assertThat(difference(sortedSetOf(1, 2, 3), setOf(2, 3, 4)),
                   is(setOf(1, 4)));
    }

    @Test
    public void testProduct() {
        assertThat(product(sortedSetOf(1, 2), sortedSetOf()), is(sortedSetOf()));
        assertThat(product(sortedSetOf(), sortedSetOf(1, 2)), is(sortedSetOf()));

        assertThat(product(sortedSetOf(5, 1, 3),
                           sortedSetOf("B", "A")),
                   contains(pairOf(1, "A"),
                            pairOf(1, "B"),
                            pairOf(3, "A"),
                            pairOf(3, "B"),
                            pairOf(5, "A"),
                            pairOf(5, "B")));
        assertThat(product(sortedSetOf(5, 1, 3).orderedSetBy(reverseOrder()),
                           sortedSetOf("B", "A")),
                   contains(pairOf(5, "A"),
                            pairOf(5, "B"),
                            pairOf(3, "A"),
                            pairOf(3, "B"),
                            pairOf(1, "A"),
                            pairOf(1, "B")));
        assertThat(product(sortedSetOf(5, 1, 3),
                           sortedSetOf("B", "A").orderedSetBy(reverseOrder())),
                   contains(pairOf(1, "B"),
                            pairOf(1, "A"),
                            pairOf(3, "B"),
                            pairOf(3, "A"),
                            pairOf(5, "B"),
                            pairOf(5, "A")));

        assertThat(product(sortedSetOf(1, 2), setOf()), is(sameInstance(setOf())));
        assertThat(product(setOf(), sortedSetOf(1, 2)), is(sameInstance(setOf())));
        assertThat(product(setOf(1, 2), setOf()), is(setOf()));
        assertThat(product(setOf(), setOf(1, 2)), is(setOf()));

        assertThat(product(setOf(5, 1, 3), setOf("B", "A")),
                   contains(pairOf(5, "B"),
                            pairOf(5, "A"),
                            pairOf(1, "B"),
                            pairOf(1, "A"),
                            pairOf(3, "B"),
                            pairOf(3, "A")));
    }

    @Test
    public void testSubset() {
        assertThat(subset(setOf(), setOf(1, 2)), is(true));
        assertThat(subset(setOf(1, 2), setOf(1, 2)), is(true));
        assertThat(subset(setOf(1, 2, 3), setOf(1, 2)), is(false));
        assertThat(subset(setOf(1, 5), setOf(1, 2, 3)), is(false));
    }

    @Test
    public void testDisjoint() {
        assertThat(disjoint(setOf(1, 2), setOf(3, 4)), is(true));
        assertThat(disjoint(setOf(1, 2), setOf(2, 3)), is(false));
        assertThat(disjoint(setOf(1, 2), setOf(1, 2)), is(false));
        assertThat(disjoint(setOf(1, 2), setOf(1, 2, 3)), is(false));
    }
}
