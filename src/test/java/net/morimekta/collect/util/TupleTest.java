/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect.util;

import org.junit.jupiter.api.Test;

import static net.morimekta.collect.util.Tuple.newTuple;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class TupleTest {
    @Test
    public void testSimple() {
        Tuple empty = newTuple();
        Tuple a = newTuple("foo", 4, 3.141592);
        Tuple b = newTuple("bar", 42, 2.16);
        assertThat(a.length(), is(3));
        assertThat(empty.length(), is(0));
        assertThat(a.slice(1, 1), is(newTuple(4)));
        assertThat(a.slice(1, 0), is(sameInstance(empty)));
        assertThat(a.truncate(2), is(newTuple("foo", 4)));
        assertThat(a.truncate(3), is(sameInstance(a)));
        assertThat(a.truncate(0), is(sameInstance(empty)));
        assertThat(a.extract(2, 1), is(newTuple(3.141592, 4)));
        assertThat(a.appended(42L), is(newTuple("foo", 4, 3.141592, 42L)));
        assertThat(a.appendedFrom(b), is(newTuple("foo", 4, 3.141592, "bar", 42, 2.16)));
        assertThat(empty.appendedFrom(a), is(sameInstance(a)));
        assertThat(empty.appended("foo", 4, 3.141592), is(a));
        assertThat(empty.appended(a), is(newTuple(a)));
        assertThat(a.appendedFrom(empty), is(sameInstance(a)));
        assertThat(a.with(2, 2.16), is(newTuple("foo", 4, 2.16)));
        assertThat(a.with(0, "bar",
                          1, 42), is(newTuple("bar", 42, 3.141592)));
        assertThat(a.with(2, "bar",
                          0, 42,
                          1, 2.16), is(newTuple(42, 2.16, "bar")));
        assertThat(a.get(1), is(4));
        assertThat(a.length(), is(3));
        assertThat(a.get(String.class, 0), is("foo"));
        assertThat(a.toString(), is("Tuple{foo, 4, 3.141592}"));
        assertThat(a.hashCode(), is(newTuple("foo", 4, 3.141592).hashCode()));
        assertThat(a.hashCode(), is(not(b.hashCode())));
    }

    @Test
    public void testBadArg() {
        Tuple a = newTuple("foo", 4, 3.141592);
        try {
            a.appended();
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not appending any values"));
        }
        try {
            a.with(0, "foo", 0, "bar");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Replacing same entry twice: 0"));
        }
        try {
            a.with(0, "foo", 0, "bar", 1, "baz");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Replacing same entry more than once: [0, 0, 1]"));
        }
        try {
            a.with(0, "foo", 1, "bar", 0, "baz");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Replacing same entry more than once: [0, 1, 0]"));
        }
        try {
            a.with(0, "foo", 1, "bar", 1, "baz");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Replacing same entry more than once: [0, 1, 1]"));
        }
        try {
            a.with(1, "foo", 1, "bar", 1, "baz");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Replacing same entry more than once: [1, 1, 1]"));
        }
        try {
            a.truncate(4);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Truncate to 4 of tuple length 3"));
        }
        try {
            a.slice(-1, 1);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid slice [off=-1, len=1] of len=3"));
        }
        try {
            a.slice(4, 1);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid slice [off=4, len=1] of len=3"));
        }
        try {
            a.slice(1, 3);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid slice [off=1, len=3] of len=3"));
        }
        try {
            a.slice(1, -1);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid slice [off=1, len=-1] of len=3"));
        }
        try {
            a.get(Integer.TYPE, 0);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Cannot cast java.lang.String to int at index 0"));
        }
        try {
            a.get(-1);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("-1 outside bounds of [0..3)"));
        }
        try {
            a.get(3);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("3 outside bounds of [0..3)"));
        }
    }
}
