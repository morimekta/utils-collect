/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.function.Predicate;

import static java.util.Collections.reverseOrder;
import static net.morimekta.collect.UnmodifiableCollection.unmodifiable;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class UnmodifiableCollectionTest {
    @Test
    public void testUnmodifiable() {
        UnmodifiableList<String> a = listOf("a");
        UnmodifiableSet<String> b = setOf("b");

        assertThat(unmodifiable(a), is(sameInstance(a)));
        assertThat(unmodifiable(b), is(sameInstance(b)));
        assertThat(unmodifiable(new ArrayList<>()), is(sameInstance(listOf())));
        assertThat(unmodifiable(new ArrayList<>(a)), is(a));
    }

    @Test
    public void testAppend() {
        UnmodifiableList<String> from = listOf("foo", "bar");
        assertThat(from.append("baz"), is(listOf("foo", "bar", "baz")));
        assertThat(listOf().append("foo"), is(listOf("foo")));

        assertThat(from.append(listOf("fizz", "buzz")), is(listOf("foo", "bar", "fizz", "buzz")));
        assertThat(from.append(listOf()), is(sameInstance(from)));
        assertThat(listOf().append(from), is(sameInstance(from)));

        assertThat(from.append("fizz", "buzz"), is(listOf("foo", "bar", "fizz", "buzz")));
        assertThat(from.append(), is(sameInstance(from)));
        assertThat(listOf().append("fizz", "buzz"), is(listOf("fizz", "buzz")));
    }

    @Test
    public void testSorted() {
        UnmodifiableList<String> a = listOf("a");

        assertThat(listOf("b", "a", "c").sorted(), is(listOf("a", "b", "c")));
        assertThat(a.sorted(), is(sameInstance(a)));
        assertThat(listOf().sorted(), is(sameInstance(listOf())));
        assertThat(setOf(5, 4, 8).sortedBy(reverseOrder()), is(listOf(8, 5, 4)));
        assertThat(a.sortedBy(reverseOrder()), is(sameInstance(a)));
        assertThat(setOf("a").sortedBy(reverseOrder()), is(listOf("a")));
        assertThat(setOf().sortedBy(reverseOrder()), is(sameInstance(listOf())));
    }

    @Test
    public void testSortedBy() {
        UnmodifiableList<String> tmp = listOf("b", "a", "c");
        assertThat(tmp.sortedBy(null), is(listOf("a", "b", "c")));
        assertThat(tmp.sortedBy(reverseOrder()), is(listOf("c", "b", "a")));
        ;
        UnmodifiableList<String> a = listOf("a");
        assertThat(a.sortedBy(reverseOrder()), is(sameInstance(a)));
        ;
    }

    @Test
    public void testMap() {
        UnmodifiableList<String> tmp = listOf("b", "a", "c");
        assertThat(listOf().map(s -> "a" + s), is(listOf()));
        assertThat(tmp.map(s -> s + "a"), is(listOf("ba", "aa", "ca")));
    }

    @Test
    public void testReversed() {
        assertThat(listOf().reversed(), is(listOf()));
        UnmodifiableList<String> tmp = listOf("b");
        assertThat(tmp.reversed(), is(sameInstance(tmp)));
        UnmodifiableSet<String> set = setOf("b", "c");
        assertThat(set.reversed().asList(), is(listOf("c", "b")));
        assertThat(listOf("b", "a", "c").reversed(), is(listOf("c", "a", "b")));
    }

    @Test
    public void testUnsupported_Collection() {
        try {
            listOf().add(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().remove(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().addAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().removeAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().removeIf(Predicate.isEqual(new Object()));
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().retainAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            listOf().clear();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }
}
