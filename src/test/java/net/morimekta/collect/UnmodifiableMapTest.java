/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect;


import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.reverseOrder;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableMap.toMap;
import static net.morimekta.collect.UnmodifiableMapBase.entry;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.collect.UnmodifiableSortedMap.sortedMapOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.fail;

public class UnmodifiableMapTest {
    @Test
    public void testBaseMap() {
        assertThat(UnmodifiableMap.mapOf().toString(), is("{}"));
        assertThat(UnmodifiableMap.mapOf("bar", "foo").toString(), is("{bar: foo}"));
        assertThat(UnmodifiableMap.mapOf("bar", "foo", "foo", "bar").toString(), is("{bar: foo, foo: bar}"));
        assertThat(UnmodifiableMap.mapOf("bar", "foo").hashCode(), is(UnmodifiableMap.mapOf("bar", "foo").hashCode()));
        assertThat(UnmodifiableMap.mapOf("bar", "foo").hashCode(),
                   is(not(UnmodifiableMap.mapOf("bar", "bar").hashCode())));
        UnmodifiableMap<String, String> base = mapOf("bar", "foo");
        assertThat(base.hashCode(), is(base.hashCode()));
        assertThat(base.keySet(), is(sameInstance(base.keySet())));
        assertThat(base.values(), is(sameInstance(base.values())));

        assertThat(UnmodifiableMap.mapOf().equals(UnmodifiableMap.mapOf()), is(true));
        assertThat(UnmodifiableMap.mapOf("foo", "bar").equals(UnmodifiableMap.mapOf("foo", "bar")),
                   is(true));
        assertThat(UnmodifiableMap.mapOf("foo", "bar")
                                  .equals(UnmodifiableMap.mapOf("foo", "bar", "baz", "baf")),
                   is(false));
        assertThat(UnmodifiableMap.mapOf().equals(new Object()), is(false));
        assertThat(UnmodifiableMap.mapOf("foo", "bar").equals(new HashMap<String, String>() {{put("foo", "bar");}}),
                   is(true));
        assertThat(UnmodifiableMap.mapOf("foo", "bar").equals(new HashMap<String, String>() {{put("foo", "foo");}}),
                   is(false));
        assertThat(UnmodifiableMap.mapOf("foo", "bar").equals(new HashMap<String, String>() {{put("foo", null);}}),
                   is(false));
        assertThat(UnmodifiableMap.mapOf("foo", "bar").equals(new HashMap<String, String>() {{put("bar", "bar");}}),
                   is(false));

        assertThat(UnmodifiableMap.mapOf("foo", "bar").equals(UnmodifiableMap.mapOf("foo", "bar")), is(true));
        assertThat(UnmodifiableMap.mapOf("foo", "bar").equals(UnmodifiableMap.mapOf("foo", "foo")), is(false));
        assertThat(UnmodifiableMap.mapOf("foo", "bar").equals(UnmodifiableMap.mapOf("bar", "bar")), is(false));
        assertThat(UnmodifiableMap.mapOf("foo", "bar").equals(UnmodifiableMap.mapOf()), is(false));
        assertThat(UnmodifiableMap.mapOf("foo", "bar").containsValue("bar"), is(true));
        assertThat(UnmodifiableMap.mapOf("foo", "bar").containsValue("foo"), is(false));

        assertThat(UnmodifiableMap.mapOf("foo", "bar", "bar", "foo").keySet(),
                   contains("foo", "bar"));
        assertThat(UnmodifiableMap.mapOf("foo", "bar", "bar", "foo").values(),
                   contains("bar", "foo"));

        assertThat(UnmodifiableMap.mapOf("foo", "bar", "bar", "baz", "baz", "foo").containsKey("foo"), is(true));
        assertThat(UnmodifiableMap.mapOf("foo", "bar", "bar", "baz", "baz", "foo", "fai", "fom").containsKey("fom"),
                   is(false));

        UnmodifiableMap<String, String> map = UnmodifiableMap.mapOf("foo",
                                                                    "bar",
                                                                    "bar",
                                                                    "foo",
                                                                    "morimekta.net",
                                                                    "providence");
        assertThat(map.orderedBy(naturalOrder()).keySet(),
                   contains("bar", "foo", "morimekta.net"));

        UnmodifiableSortedMap<String, String> sorted = sortedMapOf("foo",
                                                                   "bar",
                                                                   "bar",
                                                                   "foo",
                                                                   "morimekta.net",
                                                                   "providence");
        assertThat(sorted.orderedBy(naturalOrder()), is(sameInstance(sorted)));
        assertThat(sorted.orderedBy(reverseOrder()).keySet(),
                   contains("morimekta.net", "foo", "bar"));
        assertThat(UnmodifiableMap.<String, String>mapOf()
                           .orderedBy(Comparator.naturalOrder())
                , is(sameInstance(sortedMapOf())));
        assertThat(UnmodifiableSortedMap.<String, String>sortedMapOf()
                           .orderedBy(Comparator.naturalOrder()),
                   is(sameInstance(sortedMapOf())));
        assertThat(UnmodifiableSortedMap.sortedMapOf("foo", "bar")
                                        .orderedBy(Comparator.reverseOrder())
                                        .orderedBy(Comparator.reverseOrder())
                                        .orderedBy(Comparator.naturalOrder()),
                   is(sortedMapOf("foo", "bar")));
    }

    @Test
    public void testCollectors() {
        List<String> values = listOf("abba", "berit", "cumbria");
        Map<String, String> map = values.stream()
                                        .collect(toMap(str -> str.substring(0, 2)));
        assertThat(map, is(UnmodifiableMap.mapOf("ab", "abba", "be", "berit", "cu", "cumbria")));
        Map<String, Integer> map2 = values.stream().collect(toMap(str -> str, String::length));
        assertThat(map2, is(UnmodifiableMap.mapOf("abba", 4, "berit", 5, "cumbria", 7)));

        Map<Long, UUID> longMap = Stream.generate(UUID::randomUUID)
                                         .parallel()
                                         .limit(4096)
                                         .collect(toMap(UUID::getLeastSignificantBits));
        assertThat(longMap.size(), is(4096));
        assertThat(longMap.keySet(), hasSize(4096));
        assertThat(longMap.values(), hasSize(4096));
    }

    @Test
    public void testConstructor_Of() {
        assertThat(UnmodifiableMap.mapOf(), is(sameInstance(UnmodifiableMap.mapOf())));
        assertThat(UnmodifiableMap.mapOf().entrySet(), is(sameInstance(setOf())));
        assertThat(UnmodifiableMap.mapOf().keySet(), is(sameInstance(setOf())));
        assertThat(UnmodifiableMap.mapOf().values(), is(sameInstance(listOf())));
        assertThat(UnmodifiableMap.mapOf().isEmpty(), is(true));

        assertThat(UnmodifiableMap.mapOf("1", "1").keySet(), contains("1"));
        assertThat(UnmodifiableMap.mapOf("1", "1").values(), contains("1"));

        assertThat(UnmodifiableMap.mapOf("1", "2", "2", "1").keySet(), contains("1", "2"));
        assertThat(UnmodifiableMap.mapOf("1", "2", "2", "1").values(), contains("2", "1"));

        assertThat(UnmodifiableMap.mapOf("1", "3", "2", "2", "3", "1").keySet(), contains("1", "2", "3"));
        assertThat(UnmodifiableMap.mapOf("1", "3", "2", "2", "3", "1").values(), contains("3", "2", "1"));

        assertThat(UnmodifiableMap.mapOf("1", "4", "2", "3", "3", "2", "4", "1").keySet(), contains("1", "2", "3", "4"));
        assertThat(UnmodifiableMap.mapOf("1", "4", "2", "3", "3", "2", "4", "1").values(), contains("4", "3", "2", "1"));

        assertThat(UnmodifiableMap.mapOf("1", "5", "2", "4", "3", "3", "4", "2", "5", "1").keySet(),
                   contains("1", "2", "3", "4", "5"));
        assertThat(UnmodifiableMap.mapOf("1", "5", "2", "4", "3", "3", "4", "2", "5", "1").values(),
                   contains("5", "4", "3", "2", "1"));

        assertThat(UnmodifiableMap.mapOf("1", "6", "2", "5", "3", "4", "4", "3", "5", "2",
                                         "6", "1").keySet(),
                   contains("1", "2", "3", "4", "5", "6"));
        assertThat(UnmodifiableMap.mapOf("1", "6", "2", "5", "3", "4", "4", "3", "5", "2",
                                         "6", "1").values(),
                   contains("6", "5", "4", "3", "2", "1"));

        assertThat(UnmodifiableMap.mapOf("1", "7", "2", "6", "3", "5", "4", "4", "5", "3",
                                         "6", "2", "7", "1").keySet(),
                   contains("1", "2", "3", "4", "5", "6", "7"));
        assertThat(UnmodifiableMap.mapOf("1", "7", "2", "6", "3", "5", "4", "4", "5", "3",
                                         "6", "2", "7", "1").values(),
                   contains("7", "6", "5", "4", "3", "2", "1"));

        assertThat(UnmodifiableMap.mapOf("1", "8", "2", "7", "3", "6", "4", "5", "5", "4",
                                         "6", "3", "7", "2", "8", "1").keySet(),
                   contains("1", "2", "3", "4", "5", "6", "7", "8"));
        assertThat(UnmodifiableMap.mapOf("1", "8", "2", "7", "3", "6", "4", "5", "5", "4",
                                         "6", "3", "7", "2", "8", "1").values(),
                   contains("8", "7", "6", "5", "4", "3", "2", "1"));

        assertThat(UnmodifiableMap.mapOf("1", "9", "2", "8", "3", "7", "4", "6", "5", "5",
                                         "6", "4", "7", "3", "8", "2", "9", "1").keySet(),
                   contains("1", "2", "3", "4", "5", "6", "7", "8", "9"));
        assertThat(UnmodifiableMap.mapOf("1", "9", "2", "8", "3", "7", "4", "6", "5", "5",
                                         "6", "4", "7", "3", "8", "2", "9", "1").values(),
                   contains("9", "8", "7", "6", "5", "4", "3", "2", "1"));

        assertThat(UnmodifiableMap.mapOf("1", "10", "2", "9", "3", "8", "4", "7", "5", "6",
                                         "6", "5", "7", "4", "8", "3", "9", "2", "10", "1").keySet(),
                   contains("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"));
        assertThat(UnmodifiableMap.mapOf("1", "10", "2", "9", "3", "8", "4", "7", "5", "6",
                                         "6", "5", "7", "4", "8", "3", "9", "2", "10", "1").values(),
                   contains("10", "9", "8", "7", "6", "5", "4", "3", "2", "1"));
    }

    @Test
    public void testConstructor_CopyOf() {
        assertThat(UnmodifiableMap.asMap(new HashMap<>()),
                   is(sameInstance(UnmodifiableMap.mapOf())));
        Map<String,String> tmp = UnmodifiableMap.mapOf("a", "b");
        assertThat(UnmodifiableMap.asMap(tmp), is(sameInstance(tmp)));
        UnmodifiableSortedMap<String,String> tmp2 = sortedMapOf("a", "b");
        assertThat(UnmodifiableMap.asMap(tmp2), is(UnmodifiableMap.mapOf("a", "b")));
        assertThat(UnmodifiableMap.asMap(tmp2).entries, is(sameInstance(tmp2.entries)));
        Map<String,String> other = new HashMap<>(tmp);
        assertThat(UnmodifiableMap.asMap(other), is(tmp));
    }

    @Test
    public void testBuilder() {
        UnmodifiableMap.Builder<String,String> builder = UnmodifiableMap.newBuilder(5);
        builder.putAll();
        builder.putAll(listOf());
        builder.putAll(UnmodifiableMap.mapOf("A", "B"));
        builder.put(entry("B", "NOT"));
        builder.put("B", "C");
        Map<String, String> instance = builder.build();
        assertThat(instance.size(), is(2));
        assertThat(instance, hasEntry("A", "B"));
        assertThat(instance, hasEntry("B", "C"));
        assertThat(instance.get("A"), is("B"));
        assertThat(instance.get("C"), is(nullValue()));

        assertThat(UnmodifiableMap.newBuilder().build(), is(sameInstance(UnmodifiableMap.mapOf())));

        UnmodifiableMap.Builder<String, Integer> builder1 = UnmodifiableMap.newBuilder();
        for (int i = 0; i < 1020; ++i) {
            assertThat(builder1.put(UUID.randomUUID().toString(), i), is(sameInstance(builder1)));
        }
        assertThat(builder1.putAll(entry(UUID.randomUUID().toString(), 1020),
                        entry(UUID.randomUUID().toString(), 1021)), is(sameInstance(builder1)));
        assertThat(builder1.putAll(listOf(entry(UUID.randomUUID().toString(), 1022),
                               entry(UUID.randomUUID().toString(), 1023))),
                is(sameInstance(builder1)));

        assertThat(builder1.build().keySet(), hasSize(1024));
        assertThat(builder1.build().values(), hasSize(1024));
    }

    @Test
    public void testMap() {
        Set<String>                           keys    = new HashSet<>();
        UnmodifiableMap.Builder<String, Long> builder = UnmodifiableMap.newBuilder();
        for (int i = 0; i < 1000; ++i) {
            String uuid = UUID.randomUUID().toString();
            keys.add(uuid);
            builder.put(uuid, new Random().nextLong());
        }
        UnmodifiableMap<String, Long> map = builder.build();
        for (String uuid : keys) {
            assertThat(map.get(uuid), is(notNullValue()));
        }
        for (int i = 0; i < 1000; ++i) {
            assertThat(map.get(UUID.randomUUID().toString()), is(nullValue()));
        }

        for (int i = 0; i < 1000; ++i) {
            assertThat(UnmodifiableMap.mapOf().get(UUID.randomUUID().toString()), is(nullValue()));
        }
    }

    @Test
    public void testWithEntry() {
        UnmodifiableMap<String, Integer> from = mapOf("foo", 1, "bar", 2);
        assertThat(from.withEntry("baz", 3), is(mapOf("foo", 1, "bar", 2, "baz", 3)));
        assertThat(from.withEntry("foo", 3), is(mapOf("foo", 3, "bar", 2)));
        assertThat(mapOf().withEntry("foo", 1), is(mapOf("foo", 1)));
    }

    @Test
    public void testWithEntries() {
        UnmodifiableMap<String, Integer> from = mapOf("foo", 1, "bar", 2);
        assertThat(from.withEntries(mapOf("baz", 3)), is(mapOf("foo", 1, "bar", 2, "baz", 3)));
        assertThat(from.withEntries(mapOf("foo", 3)), is(mapOf("foo", 3, "bar", 2)));
        assertThat(mapOf().withEntries(from), is(sameInstance(from)));
        assertThat(from.withEntries(mapOf()), is(sameInstance(from)));
    }

    @Test
    public void testUnsupported_Map() {
        try {
            UnmodifiableMap.mapOf().put(new Object(), new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableMap.mapOf().remove(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableMap.mapOf().putAll(new HashMap<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableMap.mapOf().clear();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }
}
