/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect;

import com.google.common.collect.Iterables;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static java.util.Collections.enumeration;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableSet.asSet;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.collect.UnmodifiableSet.toSet;
import static net.morimekta.collect.UnmodifiableSortedSet.sortedSetOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.fail;

public class UnmodifiableSetTest {
    @Test
    public void testJava21() {
        var test = setOf();
        // java 21.
        assertThat(test.reversed(), is(sameInstance(test)));

        var test1 = setOf("foo");
        assertThat(test1.reversed(), is(sameInstance(test1)));
    }

    @Test
    public void testConvenience() {
        assertThat(asSet("a", "b"),
                   is(setOf("a", "b")));
        assertThat(IntStream.range(2, 5).boxed().collect(toSet()),
                   is(setOf(2, 3, 4)));
        assertThat(IntStream.range(0, 4096).parallel().boxed().collect(toSet()),
                   hasSize(4096));
    }

    @Test
    public void testConstructor_Of() {
        assertThat(setOf(), is(sameInstance(setOf())));
        assertThat(setOf().equals(setOf()), is(true));
        assertThat(setOf("foo").equals(setOf("foo")), is(true));
        assertThat(setOf("foo").equals(setOf("bar")), is(false));
        assertThat(setOf("foo").equals(setOf("foo", "bar")), is(false));
        assertThat(setOf().asList(), is(sameInstance(listOf())));
        assertThat(setOf(), hasSize(0));
        assertThat(setOf().isEmpty(), is(true));
        assertThat(setOf().iterator().hasNext(), is(false));
        assertThat(setOf().contains(new Object()), is(false));
        assertThat(setOf().contains(null), is(false));
        assertThat(setOf("foo").equals(new HashSet<String>() {{add("foo");}}), is(true));
        assertThat(setOf("foo").equals(new ArrayList<String>() {{add("foo");}}), is(false));
        assertThat(setOf("foo", "bar").equals(new HashSet<String>() {{
            add("foo");
            add("bar");
        }}), is(true));
        assertThat(setOf("bar", "foo").equals(new HashSet<String>() {{
            add("foo");
            add("bar");
        }}), is(true));
        assertThat(setOf("foo", "bar").equals(new TreeSet<String>() {{
            add("foo");
            add("bar");
        }}), is(true));
        assertThat(setOf("bar", "foo").equals(new TreeSet<String>() {{
            add("foo");
            add("bar");
        }}), is(true));
        assertThat(setOf("bar", "foo").toString(), is("{bar, foo}"));
        assertThat(setOf("bar", "foo").hashCode(), is(setOf("bar", "foo").hashCode()));
        assertThat(setOf("bar", "foo").hashCode(), is(not(setOf("bar", "bar").hashCode())));
        UnmodifiableSet<String> tmp = setOf("bar", "foo");
        assertThat(tmp.hashCode(), is(tmp.hashCode()));
        assertThat(setOf("foo").contains("foo"), is(true));
        assertThat(setOf("foo"), contains("foo"));
        assertThat(setOf("foo", "bar"),
                   contains("foo", "bar"));
        assertThat(setOf("foo", "bar").asList(),
                   contains("foo", "bar"));
        assertThat(setOf("foo", "bar", "baz"),
                   contains("foo", "bar", "baz"));
        assertThat(setOf("foo", "bar", "baz", "boo"),
                   contains("foo", "bar", "baz", "boo"));
        assertThat(setOf("foo", "bar", "baz", "boo", "bin"),
                   contains("foo", "bar", "baz", "boo", "bin"));
        assertThat(setOf("foo", "bar", "baz", "boo", "bin", "ble"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble"));
        assertThat(setOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa"));
        assertThat(setOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee"));
        assertThat(setOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo"));
        assertThat(setOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo", "now"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo", "now"));
        assertThat(setOf("a", "b", "c").toArray(String[]::new),
                   is(new String[]{"a", "b", "c"}));
    }

    @Test
    public void testContains() {
        String uuid = UUID.randomUUID().toString();
        UnmodifiableSet<String> set = setOf(
                uuid, UUID.randomUUID().toString());

        assertThat(set.contains(uuid), is(true));
        assertThat(set.contains(UUID.randomUUID().toString()), is(false));

        assertThat(setOf(UUID.randomUUID()).contains(UUID.randomUUID()), is(false));
    }

    @Test
    public void testFiltered() {
        UnmodifiableSet<String> ab = setOf("a", "b");
        assertThat(setOf().filtered("a"::equals), is(setOf()));
        assertThat(ab.filtered("a"::equals), is(setOf("a")));
        assertThat(ab.filtered(s -> !"".equals(s)), is(sameInstance(ab)));
    }

    @Test
    public void testConstructor_AsSet() {
        assertThat(asSet(),
                   is(sameInstance(setOf())));
        assertThat(asSet(Collections.emptyEnumeration()),
                   is(sameInstance(setOf())));
        assertThat(asSet(Collections.emptyIterator()),
                   is(sameInstance(setOf())));
        assertThat(asSet((Iterable<?>) Collections.EMPTY_LIST),
                   is(sameInstance(setOf())));
        assertThat(asSet(Collections.EMPTY_LIST),
                   is(sameInstance(setOf())));
        assertThat(asSet((Iterable<?>) listOf()),
                   is(sameInstance(setOf())));
        assertThat(asSet(listOf()),
                   is(sameInstance(setOf())));
        assertThat(asSet(Iterables.unmodifiableIterable(new LinkedList<>())),
                   is(sameInstance(setOf())));

        assertThat(asSet("a", "b", "c"),
                   is(setOf("a", "b", "c")));
        assertThat(asSet(enumeration(listOf("a", "b", "c"))),
                   is(setOf("a", "b", "c")));
        assertThat(asSet(listOf("a", "b", "c")),
                   is(setOf("a", "b", "c")));
        assertThat(asSet(List.of("a", "b", "c")),
                   is(setOf("a", "b", "c")));
        assertThat(asSet(listOf("a", "b", "c").iterator()),
                   is(setOf("a", "b", "c")));

        assertThat(asSet(Set.of("foo")),
                   is(setOf("foo")));
        assertThat(asSet(sortedSetOf("foo", "bar")),
                   is(setOf("bar", "foo")));
    }

    @Test
    public void testBuilder() {
        UnmodifiableSet.Builder<String> builder = UnmodifiableSet.newBuilder(5);
        assertThat(builder.addAll(Arrays.asList("a", "c")), is(sameInstance(builder)));
        assertThat(builder.add("b"), is(sameInstance(builder)));
        assertThat(builder.addAll(), is(sameInstance(builder)));
        assertThat(builder.addAll("e", "f"), is(sameInstance(builder)));
        assertThat(builder.addAll(Iterables.unmodifiableIterable(listOf("g"))),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(listOf("b", "d").iterator()),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(listOf()), is(sameInstance(builder)));
        assertThat(builder.addAll(enumeration(listOf("i"))),
                   is(sameInstance(builder)));
        assertThat(builder.build(),
                   contains("a", "c", "b", "e", "f", "g", "d", "i"));
    }

    @Test
    public void testUnsupported_Collection() {
        try {
            setOf().add(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            setOf().remove(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            setOf().addAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            setOf().removeAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            setOf().removeIf(Predicate.isEqual(new Object()));
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            setOf().retainAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            setOf().clear();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }
}
