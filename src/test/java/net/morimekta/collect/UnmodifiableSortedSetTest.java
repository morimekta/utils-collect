/*
 * Copyright 2020 Collect Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.collect;

import com.google.common.collect.Iterables;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.NavigableSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Comparator.reverseOrder;
import static net.morimekta.collect.UnmodifiableSortedSet.asSortedSet;
import static net.morimekta.collect.UnmodifiableSortedSet.asSortedSetOrderedBy;
import static net.morimekta.collect.UnmodifiableSortedSet.sortedSetOf;
import static net.morimekta.collect.UnmodifiableSortedSet.toSortedSet;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class UnmodifiableSortedSetTest {
    @Test
    public void testConvenience() {
        assertThat(asSortedSet("a", "c", "b"),
                   contains("a", "b", "c"));
        assertThat(asSortedSet(Set.of("a", "c", "b")),
                   contains("a", "b", "c"));
        assertThat(asSortedSetOrderedBy(reverseOrder(), "a", "c", "b"),
                   contains("c", "b", "a"));
        assertThat(asSortedSetOrderedBy(null, new String[0]),
                   is(Set.of()));
        assertThat(IntStream.range(2, 5).boxed().collect(toSortedSet()),
                   contains(2, 3, 4));
        assertThat(IntStream.range(2, 5).boxed()
                            .collect(toSortedSet(reverseOrder())),
                   contains(4, 3, 2));
        assertThat(IntStream.range(0, 8192).parallel().boxed().collect(toSortedSet()),
                   hasSize(8192));
    }

    @Test
    public void testCompacting() {
        Integer[] ints = IntStream.range(0, 8192)
                                  .map(i -> i % 2)
                                  .boxed()
                                  .toArray(Integer[]::new);
        assertThat(ints.length, is(8192));
        UnmodifiableSortedSet<Integer> set = UnmodifiableSortedSet.asSortedSet(ints);
        assertThat(set.array.length, is(2));
    }

    @Test
    @SuppressWarnings("DoubleBraceInitialization")
    public void testConstructor_Of() {
        assertThat(sortedSetOf(), is(sameInstance(sortedSetOf())));
        assertThat(sortedSetOf().equals(sortedSetOf()), is(true));
        assertThat(sortedSetOf().asList(), is(sameInstance(UnmodifiableList.listOf())));
        assertThat(sortedSetOf(), hasSize(0));
        assertThat(sortedSetOf().isEmpty(), is(true));
        assertThat(sortedSetOf().iterator().hasNext(), is(false));
        assertThat(sortedSetOf().contains(new Object()), is(false));
        assertThat(sortedSetOf().contains(null), is(false));
        assertThat(sortedSetOf("foo").equals(new HashSet<String>() {{
            add("foo");
        }}), is(true));
        assertThat(sortedSetOf("foo").equals(new ArrayList<String>() {{
            add("foo");
        }}), is(false));
        assertThat(sortedSetOf("foo", "bar").equals(new HashSet<String>() {{
            add("foo");
            add("bar");
        }}), is(true));
        assertThat(sortedSetOf("bar", "foo").equals(new HashSet<String>() {{
            add("foo");
            add("bar");
        }}), is(true));
        assertThat(sortedSetOf("foo", "bar").equals(new TreeSet<String>() {{
            add("foo");
            add("bar");
        }}), is(true));
        assertThat(sortedSetOf("bar", "foo").equals(new TreeSet<String>() {{
            add("foo");
            add("bar");
        }}), is(true));
        assertThat(sortedSetOf("bar", "foo").toString(), is("{bar, foo}"));
        assertThat(sortedSetOf("bar", "foo").hashCode(), is(sortedSetOf("bar", "foo").hashCode()));
        assertThat(sortedSetOf("bar", "foo").hashCode(), is(not(sortedSetOf("bar", "bar").hashCode())));
        assertThat(sortedSetOf("foo").contains("foo"), is(true));
        assertThat(sortedSetOf("foo"), contains("foo"));
        assertThat(sortedSetOf("foo", "bar"),
                   contains("bar", "foo"));
        assertThat(sortedSetOf("foo", "bar").asList(),
                   contains("bar", "foo"));
        assertThat(sortedSetOf("foo", "bar", "baz"),
                   contains("bar", "baz", "foo"));
        assertThat(sortedSetOf("foo", "bar", "baz", "boo"),
                   contains("bar", "baz", "boo", "foo"));
        assertThat(sortedSetOf("foo", "bar", "baz", "boo", "bin"),
                   contains("bar", "baz", "bin", "boo", "foo"));
        assertThat(sortedSetOf("foo", "bar", "baz", "boo", "bin", "ble"),
                   contains("bar", "baz", "bin", "ble", "boo", "foo"));
        assertThat(sortedSetOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa"),
                   contains("bar", "baz", "bin", "ble", "boo", "bwa", "foo"));
        assertThat(sortedSetOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee"),
                   contains("bar", "baz", "bin", "ble", "boo", "bwa", "foo", "nee"));
        assertThat(sortedSetOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo"),
                   contains("bar", "baz", "bin", "ble", "boo", "bwa", "foo", "nee", "noo"));
        assertThat(sortedSetOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo", "now"),
                   contains("bar", "baz", "bin", "ble", "boo", "bwa", "foo", "nee", "noo", "now"));
        assertThat(sortedSetOf("c", "a", "b").toArray(new String[0]),
                   is(new String[]{"a", "b", "c"}));
        assertThat(sortedSetOf("a", "b", "b", "c", "c", "d"),
                   contains("a", "b", "c", "d"));
        assertThat(sortedSetOf("b", "b"),
                   contains("b"));
        UnmodifiableSortedSet<String> tmp = sortedSetOf("foo");
        assertThat(tmp.hashCode(), is(tmp.hashCode()));
        assertThat(tmp, is(not(sortedSetOf("foo", "bar"))));
        assertThat(tmp, is(not(sortedSetOf("bar"))));
    }

    @Test
    public void testConstructor_CopyOf() {
        assertThat(asSortedSet(new Object[0]),
                   is(sameInstance(sortedSetOf())));
        assertThat(asSortedSet(Collections.emptyEnumeration()),
                   is(sameInstance(sortedSetOf())));
        assertThat(asSortedSet(Collections.emptyIterator()),
                   is(sameInstance(sortedSetOf())));
        assertThat(asSortedSet((Iterable<?>) Collections.EMPTY_LIST),
                   is(sameInstance(sortedSetOf())));
        assertThat(asSortedSet(Collections.EMPTY_LIST),
                   is(sameInstance(sortedSetOf())));
        assertThat(asSortedSet((Iterable<?>) UnmodifiableList.listOf()),
                   is(sameInstance(sortedSetOf())));
        assertThat(asSortedSet(UnmodifiableList.listOf()),
                   is(sameInstance(sortedSetOf())));
        assertThat(asSortedSet(Iterables.unmodifiableIterable(new ArrayList<>())),
                   is(sameInstance(sortedSetOf())));

        Set<String> from = asSortedSet("a", "b", "c");
        assertThat(asSortedSet(from), is(sameInstance(from)));
        Set<String> from2 = new TreeSet<>(reverseOrder());
        from2.addAll(from);
        assertThat(asSortedSet(from2), is(from));
        assertThat(asSortedSet(from2).comparator(), is(reverseOrder()));
        assertThat(asSortedSet(new String[]{"a", "b", "c"}),
                   is(sortedSetOf("a", "b", "c")));
        assertThat(asSortedSet(Collections.enumeration(UnmodifiableList.listOf("a", "b", "c"))),
                   is(sortedSetOf("a", "b", "c")));
        assertThat(asSortedSet(UnmodifiableList.listOf("a", "b", "c")),
                   is(sortedSetOf("a", "b", "c")));
        assertThat(asSortedSet(Arrays.asList("a", "b", "c")),
                   is(sortedSetOf("a", "b", "c")));
        assertThat(asSortedSet(UnmodifiableList.listOf("a", "b", "c").iterator()),
                   is(sortedSetOf("a", "b", "c")));
    }

    @Test
    public void testBuilder_Natural() {
        UnmodifiableSortedSet.Builder<String> builder = UnmodifiableSortedSet.newBuilderNaturalOrder(5);
        assertThat(builder.addAll(Arrays.asList("a", "c")), is(sameInstance(builder)));
        assertThat(builder.add("b"), is(sameInstance(builder)));
        assertThat(builder.addAll(), is(sameInstance(builder)));
        assertThat(builder.addAll("e", "f"), is(sameInstance(builder)));
        assertThat(builder.addAll(Iterables.unmodifiableIterable(UnmodifiableList.listOf("g"))),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(UnmodifiableList.listOf("b", "d").iterator()),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(UnmodifiableList.listOf()), is(sameInstance(builder)));
        assertThat(builder.addAll(Collections.enumeration(UnmodifiableList.listOf("i"))),
                   is(sameInstance(builder)));
        Set<String> first = builder.build();
        assertThat(first,
                   contains("a", "b", "c", "d", "e", "f", "g", "i"));
        assertThat(builder.build(), is(sameInstance(first)));
        builder.add("h");
        assertThat(builder.build(),
                   contains("a", "b", "c", "d", "e", "f", "g", "h", "i"));
        assertThat(first,
                   contains("a", "b", "c", "d", "e", "f", "g", "i"));

        assertThat(UnmodifiableSortedSet.newBuilderNaturalOrder().build(),
                   is(sameInstance(sortedSetOf())));
    }

    @Test
    public void testBuilder_Reverse() {
        UnmodifiableSortedSet.Builder<String> builder = UnmodifiableSortedSet.newBuilderReverseOrder(5);
        assertThat(builder.addAll(Arrays.asList("a", "c")), is(sameInstance(builder)));
        assertThat(builder.add("b"), is(sameInstance(builder)));
        assertThat(builder.addAll("e", "f"), is(sameInstance(builder)));
        assertThat(builder.addAll(Iterables.unmodifiableIterable(UnmodifiableList.listOf("g"))),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(UnmodifiableList.listOf("b", "d").iterator()),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(Collections.enumeration(UnmodifiableList.listOf("i"))),
                   is(sameInstance(builder)));
        Set<String> first = builder.build();
        assertThat(first,
                   contains("i", "g", "f", "e", "d", "c", "b", "a"));
        assertThat(builder.build(), is(sameInstance(first)));
        builder.add("h");
        assertThat(builder.build(),
                   contains("i", "h", "g", "f", "e", "d", "c", "b", "a"));
        assertThat(first,
                   contains("i", "g", "f", "e", "d", "c", "b", "a"));

        assertThat(UnmodifiableSortedSet.newBuilderReverseOrder().build().comparator(),
                   is(Comparator.reverseOrder()));
    }

    @Test
    public void testSortedSet() {
        var test = sortedSetOf("b", "d", "f");
        @SuppressWarnings("DoubleBraceInitialization")
        SortedSet<String> expected = new TreeSet<>() {{
            add("b");
            add("d");
            add("f");
        }};
        assertThat(test.comparator(), is(nullValue()));
        assertThat(test.subSet("c", "e"), is(expected.subSet("c", "e")));
        assertThat(test.subSet("b", "d"), is(expected.subSet("b", "d")));
        assertThat(test.subSet("a", "f"), is(expected.subSet("a", "f")));
        assertThat(test.subSet("b", "e"), is(expected.subSet("b", "e")));
        assertThat(test.subSet("a", "a"), is(sameInstance(sortedSetOf())));
        assertThat(test.subSet("a", "g"), is(sameInstance(test)));

        assertThat(test.headSet("a"), is(expected.headSet("a")));
        assertThat(test.headSet("a").toString(), is("{}"));
        assertThat(test.headSet("b"), is(expected.headSet("b")));
        assertThat(test.headSet("b").toString(), is("{}"));
        assertThat(test.headSet("c"), is(expected.headSet("c")));
        assertThat(test.headSet("c").toString(), is("{b}"));
        assertThat(test.headSet("e"), is(expected.headSet("e")));
        assertThat(test.headSet("e").toString(), is("{b, d}"));
        assertThat(test.headSet("f"), is(expected.headSet("f")));
        assertThat(test.headSet("f").toString(), is("{b, d}"));
        assertThat(test.headSet("g"), is(expected.headSet("g")));
        assertThat(test.headSet("g").toString(), is("{b, d, f}"));

        assertThat(test.tailSet("a"), is(expected.tailSet("a")));
        assertThat(test.tailSet("a").toString(), is("{b, d, f}"));
        assertThat(test.tailSet("b"), is(expected.tailSet("b")));
        assertThat(test.tailSet("b").toString(), is("{b, d, f}"));
        assertThat(test.tailSet("c"), is(expected.tailSet("c")));
        assertThat(test.tailSet("c").toString(), is("{d, f}"));
        assertThat(test.tailSet("e"), is(expected.tailSet("e")));
        assertThat(test.tailSet("e").toString(), is("{f}"));
        assertThat(test.tailSet("f"), is(expected.tailSet("f")));
        assertThat(test.tailSet("f").toString(), is("{f}"));
        assertThat(test.tailSet("g"), is(expected.tailSet("g")));
        assertThat(test.tailSet("g").toString(), is("{}"));

        assertThat(test.first(), is("b"));
        assertThat(test.last(), is("f"));
        assertThat(sortedSetOf("a", "b", "c").first(), is("a"));
        assertThat(sortedSetOf("a", "b", "c").last(), is("c"));
        try {
            fail("no exception: " + sortedSetOf().first());
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("size == 0"));
        }
        try {
            fail("no exception: " + sortedSetOf().last());
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("size == 0"));
        }
    }

    public static Stream<Arguments> argsNavigableSet_Basics() {
        return Stream.of(
                arguments(Named.of("lower", (BiFunction<NavigableSet<String>, String, String>) NavigableSet::lower)),
                arguments(Named.of("floor", (BiFunction<NavigableSet<String>, String, String>) NavigableSet::floor)),
                arguments(Named.of("higher", (BiFunction<NavigableSet<String>, String, String>) NavigableSet::higher)),
                arguments(Named.of("ceiling",
                                   (BiFunction<NavigableSet<String>, String, String>) NavigableSet::ceiling)));
    }

    @MethodSource("argsNavigableSet_Basics")
    @ParameterizedTest
    public void testNavigableSet_Basics(BiFunction<NavigableSet<String>, String, String> f) {
        @SuppressWarnings("DoubleBraceInitialization")
        NavigableSet<String> baseline = new TreeSet<>() {{
            add("b");
            add("d");
            add("f");
        }};
        var test = UnmodifiableSortedSet.asSortedSet(baseline);
        assertThat("test[a]", f.apply(test, "a"), is(f.apply(baseline, "a")));
        assertThat("test[b]", f.apply(test, "b"), is(f.apply(baseline, "b")));
        assertThat("test[c]", f.apply(test, "c"), is(f.apply(baseline, "c")));
        assertThat("test[d]", f.apply(test, "d"), is(f.apply(baseline, "d")));
        assertThat("test[e]", f.apply(test, "e"), is(f.apply(baseline, "e")));
        assertThat("test[f]", f.apply(test, "f"), is(f.apply(baseline, "f")));
        assertThat("test[g]", f.apply(test, "g"), is(f.apply(baseline, "g")));
    }

    public static Stream<Arguments> argsNavigableSet_SubSets() {
        return Stream.of(
                arguments(Named.of("[b,f]", ss(set -> set.subSet("b", true, "f", true)))),
                arguments(Named.of("[b,f)", ss(set -> set.subSet("b", true, "f", false)))),
                arguments(Named.of("(b,f]", ss(set -> set.subSet("b", false, "f", true)))),
                arguments(Named.of("(b,f)", ss(set -> set.subSet("b", false, "f", false)))),
                arguments(Named.of("..b]", ss(set -> set.headSet("b", true)))),
                arguments(Named.of("..b)", ss(set -> set.headSet("b", false)))),
                arguments(Named.of("..f]", ss(set -> set.headSet("f", true)))),
                arguments(Named.of("..f)", ss(set -> set.headSet("f", false)))),
                arguments(Named.of("[b..", ss(set -> set.tailSet("b", true)))),
                arguments(Named.of("(b..", ss(set -> set.tailSet("b", false)))),
                arguments(Named.of("[f..", ss(set -> set.tailSet("f", true)))),
                arguments(Named.of("(f..", ss(set -> set.tailSet("f", false)))));
    }

    private static Object ss(Function<NavigableSet<String>, NavigableSet<String>> f) {
        return f;
    }

    @MethodSource("argsNavigableSet_SubSets")
    @ParameterizedTest
    public void testNavigableSet_SubSets(Function<NavigableSet<String>, NavigableSet<String>> f) {
        @SuppressWarnings("DoubleBraceInitialization")
        NavigableSet<String> baseline = new TreeSet<>() {{
            add("b");
            add("d");
            add("f");
        }};
        var test = UnmodifiableSortedSet.asSortedSet(baseline);
        assertThat(f.apply(test), is(f.apply(baseline)));
    }

    @Test
    public void testNavigableSet() {
        NavigableSet<String> baseline = new TreeSet<>() {{
            add("b");
            add("d");
            add("f");
        }};
        var test = UnmodifiableSortedSet.asSortedSet(baseline);

        var reversed = test.reversed();
        assertThat(reversed.asList(), is(List.of("f", "d", "b")));
        assertThat(reversed.reversed().asList(), is(List.of("b", "d", "f")));

        assertThat(test.descendingSet().asList(), is(List.of("f", "d", "b")));
        assertThat(test.descendingIterator().next(), is("f"));
    }

    @Test
    public void testJava21() {
        var test = sortedSetOf();
        assertThat(test.reversed(), is(not(sameInstance(test))));
        var test1 = sortedSetOf("foo");
        assertThat(test1.reversed(), is(not(sameInstance(test1))));
    }

    @Test
    public void testNavigableSet_Unsupported() {
        var test = UnmodifiableSortedSet.sortedSetOf();

        assertThrows(UnsupportedOperationException.class, test::pollFirst);
        assertThrows(UnsupportedOperationException.class, test::pollLast);
    }

    @Test
    public void testMakeCompact() {
        UnmodifiableSortedSet.Builder<String> builder = UnmodifiableSortedSet.newBuilderNaturalOrder();
        for (int i = 0; i < 2050; ++i) {
            builder.addAll("a", "b", "c");
        }
        UnmodifiableSortedSet<String> result = builder.build();
        assertThat(result.array.length, is(3));
        assertThat(result, contains("a", "b", "c"));
    }

    @Test
    public void testFiltered() {
        UnmodifiableSortedSet<String> ab = sortedSetOf("a", "b");
        assertThat(sortedSetOf().filtered("a"::equals), is(sortedSetOf()));
        assertThat(ab.filtered("a"::equals), is(sortedSetOf("a")));
        assertThat(ab.filtered(s -> !"".equals(s)), is(sameInstance(ab)));
    }

    @SuppressWarnings("JdkObsolete")
    @Test
    public void testUnsupported_Collection() {
        try {
            sortedSetOf().add(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            sortedSetOf().remove(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            sortedSetOf().addAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            sortedSetOf().removeAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            sortedSetOf().removeIf(Predicate.isEqual(new Object()));
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            sortedSetOf().retainAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            sortedSetOf().clear();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }
}
